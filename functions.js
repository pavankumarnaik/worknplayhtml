function serviceImageLoadCompletion(e) {
    $(e).parent().parent().children('.loading').removeClass('loading');
    $(e).css('display', 'block');
    $(e).parent().siblings().children().css('display', 'block');
}

function blogImageLoadCompletion(e) {
    let parent = $(e).parent();
    parent.removeClass('loading');

    let siblings = parent.parent().siblings();
    siblings.children('.loading').removeClass('loading');
    $(e).css('display', 'block');

    siblings.find('.blog-title').css('display', 'block');
    siblings.find('.blogtime').css('display', 'block');
    siblings.find('.blog-descriptions').css('display', 'block');
}

function latestBlogImageLoadCompletion(e) {
    let parent = $(e).parent();
    parent.removeClass('loading');

    let siblings = parent.parent().siblings();
    siblings.children('.loading').removeClass('loading');
    $(e).css('display', 'block');

    siblings.find('.latestblog-title').css('display', 'block');
    siblings.find('.blogtime').css('display', 'block');
}

function galleryImageLoadCompletion(e) {
    $(e).parent().parent().children('.loading').removeClass('loading');
    $(e).css('display', 'block');
    $(e).parent().siblings().children().css('display', 'flex');
}

function galleryPreviewImageLoadCompletion(e) {
    $(e).parent().removeClass('loading');
    $(e).css('display', 'block');
}

const linkRegEx = /((?:(http|https|Http|Https|rtsp|Rtsp):\/\/(?:(?:[a-zA-Z0-9\$\-\_\.\+\!\*\'\(\)\,\;\?\&\=]|(?:\%[a-fA-F0-9]{2})){1,64}(?:\:(?:[a-zA-Z0-9\$\-\_\.\+\!\*\'\(\)\,\;\?\&\=]|(?:\%[a-fA-F0-9]{2})){1,25})?\@)?|(([a-zA-Z0-9\-\_\.])+@)?)?((?:(?:[a-zA-Z0-9][a-zA-Z0-9\-]{0,64}\.)+(?:(?:aero|arpa|asia|a[cdefgilmnoqrstuwxz])|(?:biz|b[abdefghijmnorstvwyz])|(?:cat|com|coop|c[acdfghiklmnoruvxyz])|d[ejkmoz]|(?:edu|e[cegrstu])|f[ijkmor]|(?:gov|g[abdefghilmnpqrstuwy])|h[kmnrtu]|(?:info|int|i[delmnoqrst])|(?:jobs|j[emop])|k[eghimnrwyz]|l[abcikrstuvy]|(?:mil|mobi|museum|m[acdghklmnopqrstuvwxyz])|(?:name|net|n[acefgilopruz])|(?:org|om)|(?:pro|p[aefghklmnrstwy])|qa|r[eouw]|s[abcdeghijklmnortuvyz]|(?:tel|travel|t[cdfghjklmnoprtvwz])|u[agkmsyz]|v[aceginu]|w[fs]|y[etu]|z[amw]))|(?:(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])))(?:\:\d{1,5})?)(\/(?:(?:[a-zA-Z0-9\;\/\?\:\@\&\=\#\~\-\.\+\!\*\'\(\)\,\_])|(?:\%[a-fA-F0-9]{2}))*)?(?:\b|$)(\/)?/gim;
const emailRegEx = /\S+@\S+\.\S+/;

function linkify(inputText) {
    if (!inputText) {
        return "";
    }

    let result = document.createElement('pre');
    result.classList.add("inherit");

    let recursiveText = inputText.toString();
    let hyperlinks = inputText.match(linkRegEx);
        
    try {
        
        if(hyperlinks) {
            hyperlinks.forEach((link, index) => {

                let splitTextArray, beginningText, hyperlinkTag, endingText, isEmail;
    
                splitTextArray = recursiveText.split(link);
                beginningText = splitTextArray[0];
                isEmail = emailRegEx.test(link);
                hyperlinkTag = document.createElement('a');
                hyperlinkTag.href = isEmail ? `mailto:${link}` : (link.indexOf("http") === -1 ? `http://${link}` : link);
                hyperlinkTag.target = "_blank";
                hyperlinkTag.innerHTML = link;
                endingText = index === hyperlinks.length - 1 ? splitTextArray[1] : "";
    
                result.append(beginningText);
                result.append(hyperlinkTag);
                result.append(endingText);

                recursiveText = splitTextArray.filter((txt, i) => i > 0).join(link);
            });
        } else {
            result.innerHTML = inputText;
        }
        
    } catch(err) {
        result.innerHTML = inputText;
        return result;
    };
    return result ;
}