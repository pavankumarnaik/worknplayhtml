var headerScript = $('script[src*=header]');

var redirectPath = headerScript.attr('data-path');  
document.write(`

<div class="d-none d-lg-block">
  <nav class="navbar navbar-expand-lg navbar-fixed" id="navbar-custom" >
      <img class="logoimg" src='./images/wellness/wellnesslogo.png'>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".collapse" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav navbarlist">

          <li class="nav-item active">
            <a href="./">Home</a>
          </li>

          <li class="nav-item">
            <a href="./#services"> Services</a>
          </li>

          <li class="nav-item">
            <a href="about-us.html">About</a>
          </li>

          <li class="nav-item">
            <a href="contact.html">Contact</a>
          </li>

          <li class="nav-item">
            <a href="blogs.html">Blogs</a>
          </li>

        </ul>
      </div>
    </nav>

    <a class="btn-joinus text-right" href="${redirectPath}">LOGIN</a>

    </div>

   
    <div class="d-block d-lg-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-white navbar-fixed-mobile" id="website-nav">

<img id="logo" class="nav-brand navimg" title="Wellness Associates Logo" src="./images/wellness/wellnesslogo.png" alt="Wellness Associates Logo" />
<div class="container navnar-width d-flex justify-content-end">
<div class="navcontent">

 <a href="${redirectPath}" class="join-us-btn-mobile  col-6">LOGIN</a>

  <button class="navbar-toggler navbar-toggler-mobile offset-1 col-2" type="button" data-toggle="collapse" data-target=".collapse" aria-controls="websitenavbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  </div>
 
  <div class="collapse" id="websitenavbarNav">
    <ul class="navbar-nav navbar-mobile text-right mr-1 " style="float: none;">
      <li class="nav-item active">
        <a  class="navtext" onclick="closeNavMenu()" href="./">Home</a>
      </li>
  
      <li class="nav-item">
        <a onclick="closeNavMenu()" href="./#services"> Services</a>
      </li>
  
      <li class="nav-item">
        <a href="about-us.html" onclick="closeNavMenu()">About</a>
      </li>
  
      <li class="nav-item">
        <a href="contact.html" onclick="closeNavMenu()">Contact</a>
      </li>
  
      <li class="nav-item">
        <a href="blogs.html" onclick="closeNavMenu()">Blogs</a>
      </li>
    </ul>

  </div>
  </div>
</nav>
        </div>


`)

