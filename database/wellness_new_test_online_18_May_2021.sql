-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 17, 2021 at 11:21 PM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wellness_new_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_details`
--

CREATE TABLE `blog_details` (
  `id` int(11) NOT NULL,
  `blogId` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blog_details`
--

INSERT INTO `blog_details` (`id`, `blogId`, `title`, `image`, `details`) VALUES
(98, 84, 'Try Zumba', '/blogimage/wellness-try-zumba-0.51239500-1616049024.jpg', '  We take the work out of workout, by mixing low-intensity and high-intensity moves for an interval-style, calorie-burning dance fitness party. Once the Latin and World rhythms take over, you will see why Zumba Fitness classes are often called exercise in disguise.'),
(99, 84, 'Energy Booster', '/blogimage/wellness-energy-booster-0.51338700-1616049024.jpg', '    A total workout, combining all elements of fitness â€“ cardio, muscle conditioning, balance and flexibility, boosted energy and a serious dose of awesome each time you leave class.'),
(100, 85, 'Healthy Foods', '/blogimage/wellness-healthy-foods-0.43061600-1616408027.jpg', 'The blog of bestselling food offers an exciting combination of healthy cooking and adventure. Having realized the dangers of overindulging in white carbohydrates as a teenager, Tori now devotes her life to making and unearthing low-carb dishes in practically every corner of the world. Her extensive archive of recipes allows readers to experience the health benefits for themselves, while the Travel section engagingly records her memories of eating out in fascinating cities.'),
(101, 85, 'Healthy Foods habit', '/blogimage/wellness-healthy-foods-habit-0.43159000-1616408027.jpg', 'If youâ€™re a vegetarian, vegan, an allergy sufferer or just a lover of wholesome food, finding the appropriate recipes can sometimes be tricky. Help is at hand, however. Coming from places such as the USA, UK these bloggers share their nutritional expertise and tasty recipes with millions of avid readers, proving how fun, inspirational and easy healthy cooking can be.'),
(102, 85, 'Wellness events', '/blogimage/wellness-wellness-events-0.43232800-1616408027.jpg', 'Coming from places such as the USA, UK these bloggers share their nutritional expertise and tasty recipes with millions of avid readers, proving how fun, inspirational and easy healthy cooking can be.'),
(106, 88, 'Yoga Magazine', '/blogimage/wellness-yoga-magazine-0.35797400-1619689725.jpg', 'More of a catch-all health and alt-living magazine than a yoga blog. Weâ€™ll just focus on the category, â€œyoga,â€ which is one subsection of the body in MindBodyGreen. Immediate points lost for the annoying newsletter pop-up'),
(107, 88, 'Yoga Library', '/blogimage/wellness-yoga-library-0.36060000-1619689725.jpg', 'Browse our extensive yoga sequence library and find a home practice that fits into your schedule. We break up our yoga sequences into levels of difficulty so youâ€™re keeping your body safe in every pose. Or search through our list of yoga sequences by anatomy to target different aspects of the body');

-- --------------------------------------------------------

--
-- Table structure for table `blog_list`
--

CREATE TABLE `blog_list` (
  `blogId` int(255) NOT NULL,
  `blogBy` varchar(255) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `htmlFiles` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blog_list`
--

INSERT INTO `blog_list` (`blogId`, `blogBy`, `timeStamp`, `htmlFiles`) VALUES
(84, 'Support', '2021-04-05 04:59:27', 'wellness-try-zumba.html'),
(85, 'Support', '2021-04-05 04:59:56', 'wellness-healthy-foods.html'),
(88, 'Support', '2021-04-29 09:48:45', 'wellness-yoga-magazine.html');

-- --------------------------------------------------------

--
-- Table structure for table `budget_master`
--

CREATE TABLE `budget_master` (
  `id` int(11) NOT NULL,
  `budget_range` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedAt` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `category_level1`
--

CREATE TABLE `category_level1` (
  `level1_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `main_description` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_level1`
--

INSERT INTO `category_level1` (`level1_id`, `name`, `main_description`) VALUES
(1, 'workshops and event', 'Workshop is simply dummy text of the printing and typesetting industry. the industry\'s standard dummy text ever since the 1500s.'),
(2, 'group activities', 'Whether you’re throwing a smaller event or a multi-thousand person conference, having fulfilled and happy attendees is key to knocking it out of the park. Which is why many planners are turning to wellness events and stations! ');

-- --------------------------------------------------------

--
-- Table structure for table `category_level2`
--

CREATE TABLE `category_level2` (
  `level2_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `level1_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_level2`
--

INSERT INTO `category_level2` (`level2_id`, `name`, `level1_id`) VALUES
(1, 'Onsite', 1),
(2, 'Virtual', 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city_name`) VALUES
(2, 'Achalpur'),
(3, 'Adilabad'),
(4, 'Adityapur'),
(5, 'Adoni'),
(7, 'Agartala'),
(6, 'Agartala MCl'),
(8, 'Agra'),
(9, 'Ahmadabad'),
(10, 'Ahmadnagar'),
(11, 'Aizawl'),
(12, 'Ajmer'),
(13, 'Akola'),
(14, 'Akot'),
(15, 'Alandur'),
(16, 'Alappuzha'),
(17, 'Aligarh'),
(19, 'Allahabad'),
(21, 'Alwar'),
(22, 'Amalner'),
(23, 'Ambajogai'),
(26, 'Ambala'),
(24, 'Ambala Cantonment'),
(25, 'Ambala Sadar'),
(27, 'Ambattur'),
(28, 'Ambikapur'),
(29, 'Ambur'),
(30, 'Amravati'),
(31, 'Amreli'),
(32, 'Amritsar'),
(33, 'Amroha'),
(34, 'Anakapalle'),
(36, 'Anantapur'),
(37, 'Anantnag'),
(38, 'Anjangaon'),
(39, 'Anjar'),
(41, 'Arakonam'),
(42, 'Arambagh'),
(43, 'Arani'),
(44, 'Araria'),
(45, 'Arrah'),
(46, 'Aruppukkottai'),
(47, 'Asansol'),
(49, 'Ashoknagar'),
(48, 'Ashoknagar Kalyangarh'),
(50, 'Attur'),
(51, 'Auraiya'),
(52, 'Aurangabad'),
(53, 'Avadi'),
(54, 'Avaniapuram'),
(55, 'Ayodhya'),
(56, 'Azamgarh'),
(57, 'Badharghat'),
(58, 'Badlapur'),
(59, 'Bagaha'),
(60, 'Bagalkot'),
(61, 'Bagbahra'),
(62, 'Bahadurgarh'),
(63, 'Baharampur'),
(64, 'Baheri'),
(65, 'Bahraich'),
(66, 'Baidyabati'),
(68, 'Balaghat'),
(69, 'Balangir'),
(70, 'Baleshwar'),
(71, 'Ballarpur'),
(72, 'Ballia'),
(73, 'Bally Cantonment'),
(75, 'Balotra'),
(76, 'Balrampur'),
(77, 'Balurghat'),
(78, 'Banda'),
(79, 'Bangalore'),
(80, 'Bangaon'),
(81, 'Bankra'),
(82, 'Bankura'),
(83, 'Bansbaria'),
(84, 'Banswara'),
(85, 'Bapatla'),
(86, 'Baramati'),
(87, 'Baramula'),
(89, 'Baranagar'),
(90, 'Baraut'),
(91, 'Barbil'),
(92, 'Barddhaman'),
(93, 'Bardoli'),
(94, 'Bareli'),
(95, 'Bargarh'),
(96, 'Bari Brahmana'),
(98, 'Baripada'),
(99, 'Barmer'),
(100, 'Barnala'),
(101, 'Barpeta'),
(102, 'Barsi'),
(103, 'Basavakalyan'),
(104, 'Basirhat'),
(105, 'Basmat'),
(106, 'Basoda'),
(107, 'Basti'),
(108, 'Batala'),
(109, 'Bathinda'),
(110, 'Beawar'),
(111, 'Begusarai'),
(112, 'Behta Hajipur'),
(114, 'Belampalli'),
(115, 'Belgaum'),
(116, 'Bellary'),
(117, 'Bettiah'),
(118, 'Betul'),
(119, 'Beypur'),
(120, 'Bhabua'),
(121, 'Bhadohi'),
(122, 'Bhadrak'),
(123, 'Bhadravati'),
(124, 'Bhadreswar'),
(125, 'Bhagalpur'),
(126, 'Bhalswa Jahangirpur'),
(127, 'Bhandara'),
(128, 'Bharatpur'),
(129, 'Bharuch'),
(130, 'Bhatpara'),
(131, 'Bhavnagar'),
(132, 'Bhawanipatna'),
(133, 'Bhilai'),
(134, 'Bhilwara'),
(135, 'Bhimavaram'),
(136, 'Bhind'),
(137, 'Bhiwadi'),
(138, 'Bhiwandi'),
(139, 'Bhiwani'),
(140, 'Bhongir'),
(141, 'Bhopal'),
(142, 'Bhubaneswar'),
(144, 'Bhuli'),
(145, 'Bhusawal'),
(147, 'Bidar'),
(148, 'Bidhannagar'),
(149, 'Bihar'),
(150, 'Bijapur'),
(151, 'Bijnor'),
(152, 'Bikaner'),
(153, 'Bilaspur'),
(154, 'BinaEtawa'),
(155, 'Binnaguri'),
(156, 'Bisalpur'),
(157, 'Bishnupur'),
(158, 'Biswan'),
(160, 'Bodhan'),
(161, 'Bodinayakkanur'),
(162, 'Bokaro'),
(163, 'Bolpur'),
(164, 'Bommanahalli'),
(165, 'Bongaigaon'),
(166, 'Borsad'),
(167, 'Botad'),
(168, 'Brahmapur'),
(169, 'Brajrajnagar'),
(170, 'Budaun'),
(171, 'Bulandshahr'),
(172, 'Buldana'),
(173, 'Bundi'),
(174, 'Burari'),
(175, 'Burhanpur'),
(177, 'Byatarayanapura'),
(178, 'Calcutta'),
(179, 'Chaibasa'),
(180, 'Chakdaha'),
(181, 'Chalisgaon'),
(182, 'Challakere'),
(183, 'Champdani'),
(184, 'Chamrajnagar'),
(185, 'Chandannagar'),
(186, 'Chandausi'),
(187, 'Chandigarh'),
(188, 'Chandkheda'),
(189, 'Chandlodiya'),
(190, 'Chandpur'),
(191, 'Chandrapur'),
(192, 'Channapatna'),
(193, 'Charkhi Dadri'),
(194, 'Charoda'),
(196, 'Chengalpattu'),
(197, 'Cheruvannur'),
(198, 'Chhapra'),
(199, 'Chhatarpur'),
(200, 'Chhibramau'),
(201, 'Chhindwara'),
(202, 'Chik Ballapur'),
(203, 'Chikhli'),
(204, 'Chikmagalur'),
(205, 'Chilakalurupet'),
(206, 'Chilla Saroda Bangar'),
(207, 'Chinna Chawk'),
(208, 'Chintamani'),
(209, 'Chiplun'),
(211, 'Chirmiri'),
(212, 'Chitradurga'),
(213, 'Chitrakut Dham'),
(214, 'Chittaurgarh'),
(215, 'Chittur'),
(216, 'Chomun'),
(219, 'Contai'),
(220, 'Cuddapah'),
(221, 'Dabgram'),
(222, 'Dabhoi'),
(223, 'Dabra'),
(224, 'Dabwali'),
(225, 'Dadri'),
(226, 'Dahanu'),
(227, 'Dahod'),
(229, 'Daltenganj'),
(230, 'Damoh'),
(232, 'Darbhanga'),
(233, 'Darjiling'),
(234, 'Dasarahalli'),
(235, 'Datia'),
(236, 'Davanagere'),
(238, 'Dehra Dun'),
(237, 'Dehra Dun Cantonment'),
(240, 'Delhi'),
(241, 'Deoband'),
(242, 'Deolali'),
(243, 'Deoli'),
(244, 'Deoria'),
(245, 'Devghar'),
(246, 'Dewas'),
(247, 'Dhamtari'),
(248, 'Dhanbad'),
(250, 'Dharapuram'),
(251, 'Dharmapuri'),
(252, 'Dharmavaram'),
(253, 'Dhaulpur'),
(254, 'Dhenkanal'),
(255, 'Dholka'),
(257, 'Dhoraji'),
(258, 'Dhrangadhra'),
(259, 'Dhuburi'),
(260, 'Dhule'),
(261, 'Dhulian'),
(262, 'Dhuri'),
(263, 'Dibrugarh'),
(264, 'Didwana'),
(265, 'Digdoh'),
(266, 'DighaMainpura'),
(267, 'Diglur'),
(268, 'Dilli Cantonment'),
(269, 'Dimapur'),
(270, 'Dinapur'),
(271, 'Dindigul'),
(272, 'Dinhata'),
(273, 'Diphu'),
(275, 'Dod Ballapur'),
(276, 'Dum Dum'),
(277, 'Dumraon'),
(279, 'Durgapur'),
(280, 'Edakkara'),
(281, 'Edathala'),
(282, 'Eluru'),
(283, 'Erode'),
(286, 'Faizabad'),
(287, 'Faridabad'),
(288, 'Faridkot'),
(289, 'Faridpur'),
(290, 'Farrukhabad'),
(291, 'Fatehpur'),
(292, 'Fazilka'),
(293, 'Firozabad'),
(295, 'Firozpur'),
(294, 'Firozpur Cantonment'),
(296, 'Gadag'),
(297, 'Gadchiroli'),
(298, 'Gaddiannaram'),
(299, 'Gadwal'),
(300, 'Gajraula'),
(301, 'Gajuwaka'),
(302, 'Gandhidham'),
(303, 'Gandhinagar'),
(304, 'Ganga Ghat'),
(305, 'Ganganagar'),
(306, 'Gangapur'),
(307, 'Gangarampur'),
(308, 'Gangawati'),
(309, 'Gangoh'),
(310, 'Garulia'),
(312, 'Gayespur'),
(313, 'Gharoli'),
(314, 'Ghatal'),
(315, 'Ghatlodiya'),
(316, 'Ghaziabad'),
(317, 'Ghazipur'),
(318, 'Gobindgarh'),
(319, 'Godhra'),
(320, 'Gohad'),
(321, 'Gohana'),
(322, 'Gokak'),
(323, 'Gokalpur'),
(324, 'Gola Gokarannath'),
(325, 'Gola Range'),
(326, 'Gonda'),
(327, 'Gondal'),
(328, 'Gondiya'),
(329, 'Gopalganj'),
(330, 'Gopalpur'),
(331, 'Gopichettipalaiyam'),
(332, 'Gorakhpur'),
(333, 'Goura'),
(334, 'Gudalur'),
(335, 'Gudivada'),
(336, 'Gudiyattam'),
(337, 'Gudur'),
(338, 'Gulbarga'),
(339, 'Guna'),
(340, 'Guntakal'),
(341, 'Guntur'),
(342, 'Gurdaspur'),
(343, 'Guwahati'),
(344, 'Gwalior'),
(345, 'Habra'),
(346, 'Hajipur'),
(347, 'Haldwani'),
(348, 'Halisahar'),
(349, 'Hanumangarh'),
(350, 'Haora'),
(351, 'Hapur'),
(352, 'Harda'),
(353, 'Hardoi'),
(354, 'Haridwar'),
(355, 'Harihar'),
(356, 'Hasanpur'),
(357, 'Hassan'),
(358, 'Hastsal'),
(359, 'Hathras'),
(360, 'Haveri'),
(361, 'Hazaribag'),
(362, 'Himatnagar'),
(363, 'Hindaun'),
(364, 'Hindupur'),
(365, 'Hinganghat'),
(366, 'Hingoli'),
(367, 'Hiriyur'),
(368, 'Hisar'),
(369, 'Hosakote'),
(370, 'Hoshangabad'),
(371, 'Hoshiarpur'),
(372, 'Hospet'),
(373, 'Hosur'),
(374, 'Hubli'),
(375, 'HugliChunchura'),
(376, 'Hyderabad'),
(377, 'Ichalkaranji'),
(378, 'Idappadi'),
(379, 'Ilkal'),
(380, 'Imphal'),
(381, 'Indore'),
(382, 'Ingraj Bazar'),
(383, 'Islampur'),
(384, 'Itanagar'),
(385, 'Itarsi'),
(387, 'Jabalpur'),
(386, 'Jabalpur Cantonment'),
(388, 'Jaffrabad'),
(389, 'Jagadhri'),
(390, 'Jagdalpur'),
(391, 'Jagraon'),
(392, 'Jagtial'),
(393, 'Jahanabad'),
(394, 'Jahangirabad'),
(395, 'Jaipur'),
(396, 'Jaisalmer'),
(397, 'Jalandhar'),
(398, 'Jalaun'),
(399, 'Jalgaon'),
(400, 'Jalna'),
(401, 'Jalor'),
(402, 'Jalpaiguri'),
(403, 'Jamalpur'),
(404, 'Jamkhandi'),
(405, 'Jammu'),
(406, 'Jamnagar'),
(407, 'Jamshedpur'),
(408, 'Jamui'),
(409, 'Jamuria'),
(410, 'Jangipur'),
(411, 'Jaora'),
(412, 'Jaunpur'),
(413, 'Jaypur'),
(414, 'Jetpur'),
(415, 'Jhalawar'),
(416, 'Jhansi'),
(417, 'Jhargram'),
(418, 'Jharia'),
(419, 'Jharsuguda'),
(420, 'Jhumri Tilaiya'),
(421, 'Jhunjhunun'),
(423, 'Jodhpur'),
(424, 'Jorapokhar'),
(425, 'Jorhat'),
(426, 'Junagadh'),
(427, 'Kadayanallur'),
(429, 'Kadiri'),
(430, 'Kagaznagar'),
(431, 'Kairana'),
(432, 'Kaithal'),
(433, 'Kakinada'),
(434, 'Kalamassery'),
(994, 'Kalinga'),
(435, 'Kaliyaganj'),
(436, 'Kallur'),
(437, 'Kalna'),
(439, 'Kalyan'),
(440, 'Kalyani'),
(441, 'Kamareddi'),
(442, 'Kamarhati'),
(443, 'Kambam'),
(444, 'Kamthi'),
(445, 'Kanakapura'),
(446, 'Kanchipuram'),
(447, 'Kanchrapara'),
(448, 'Kandi'),
(449, 'Kannan Devan Hills'),
(450, 'Kannangad'),
(451, 'Kannauj'),
(452, 'Kannur'),
(454, 'Kanpur'),
(453, 'Kanpur Cantonment'),
(455, 'Kapra'),
(456, 'Kapurthala'),
(457, 'Karaikal'),
(458, 'Karanja'),
(459, 'Karauli'),
(460, 'Karawal Nagar'),
(461, 'Karimganj'),
(462, 'Karimnagar'),
(463, 'Karnal'),
(464, 'Karnul'),
(465, 'Karsiyang'),
(466, 'Karur'),
(467, 'Karwar'),
(468, 'Kasganj'),
(469, 'Kashipur'),
(470, 'Kataka'),
(471, 'Kathua'),
(472, 'Katihar'),
(473, 'Katras'),
(474, 'Katwa'),
(475, 'Kavali'),
(476, 'Kavundampalaiyam'),
(477, 'Kayankulam'),
(478, 'Kendujhar'),
(479, 'Keshod'),
(480, 'Khadki'),
(481, 'Khagaria'),
(482, 'Khagaul'),
(483, 'Khajuri Khas'),
(484, 'Khambhat'),
(485, 'Khamgaon'),
(486, 'Khammam'),
(487, 'Khandwa'),
(488, 'Khanna'),
(490, 'Kharagpur'),
(489, 'Kharagpur Railway Settlement'),
(491, 'Khardaha'),
(492, 'Khargone'),
(493, 'Kharia'),
(494, 'Khatauli'),
(495, 'Khopoli'),
(496, 'Khora'),
(497, 'Khurja'),
(498, 'Kirari Suleman Nagar'),
(499, 'Kiratpur'),
(500, 'Kishanganj'),
(501, 'Kishangarh'),
(502, 'Koch Bihar'),
(503, 'Kochi'),
(504, 'Kodar'),
(505, 'Kohima'),
(506, 'Kolar'),
(507, 'Kolhapur'),
(508, 'Kollam'),
(509, 'Kollegal'),
(510, 'Kondukur'),
(511, 'Konnagar'),
(512, 'Kopargaon'),
(513, 'Koppal'),
(514, 'Koratla'),
(515, 'Korba'),
(516, 'Kosi Kalan'),
(517, 'Kot Kapura'),
(518, 'Kota'),
(519, 'Kottagudem'),
(520, 'Kottayam'),
(521, 'Kovilpatti'),
(522, 'Koyampattur'),
(523, 'Koyilandi'),
(524, 'Kozhikkod'),
(525, 'Krishnagiri'),
(526, 'Krishnanagar'),
(527, 'Krishnarajapura'),
(528, 'Kuchaman'),
(529, 'Kukatpalle'),
(530, 'Kulti'),
(531, 'Kumarapalaiyam'),
(532, 'Kumbakonam'),
(533, 'Kundla'),
(534, 'Kuniyamuthur'),
(535, 'Kunnamkulam'),
(536, 'Kurichi'),
(537, 'Ladnun'),
(538, 'Laharpur'),
(539, 'Lakhimpur'),
(540, 'Lakhisarai'),
(542, 'Lakhnau'),
(541, 'Lakhnau Cantonment'),
(543, 'Lalbahadur Nagar'),
(544, 'Lalitpur'),
(545, 'Lanka'),
(546, 'Latur'),
(547, 'Lohardaga'),
(548, 'Lonavale'),
(549, 'Loni'),
(550, 'Ludhiana'),
(551, 'Lunglei'),
(552, 'Machilipatnam'),
(553, 'Madgaon'),
(554, 'Madhavaram'),
(555, 'Madhipura'),
(556, 'Madhubani'),
(557, 'Madhyamgram'),
(558, 'Madras'),
(559, 'Madurai'),
(560, 'Maduravoyal'),
(561, 'Mahadevapura'),
(562, 'Mahbubnagar'),
(563, 'Maheshtala'),
(564, 'Mahoba'),
(565, 'Mahuva'),
(566, 'Mainpuri'),
(567, 'Maisuru'),
(568, 'Makrana'),
(569, 'Malappuram'),
(570, 'Malaut'),
(571, 'Malegaon'),
(572, 'Maler Kotla'),
(573, 'Malkajgiri'),
(574, 'Malkapur'),
(575, 'Mancheral'),
(576, 'Mandamarri'),
(577, 'Mandidip'),
(578, 'Mandoli'),
(579, 'Mandsaur'),
(580, 'Mandya'),
(581, 'Mangalagiri'),
(582, 'Mangaluru'),
(583, 'Mango'),
(584, 'Mangrol'),
(585, 'Manjeri'),
(586, 'Manmad'),
(587, 'Mannargudi'),
(588, 'Mansa'),
(589, 'Markapur'),
(590, 'Masaurhi'),
(591, 'Mathura'),
(592, 'Mau'),
(593, 'Mauranipur'),
(594, 'Mawana'),
(595, 'Mayiladuthurai'),
(596, 'Memari'),
(597, 'Mettupalayam'),
(598, 'Mettur'),
(599, 'Midnapur'),
(600, 'Mira Bhayandar'),
(602, 'Mirat'),
(601, 'Mirat Cantonment'),
(603, 'Miryalaguda'),
(604, 'Mirzapur'),
(1008, 'MisbaIron'),
(605, 'Mithe Pur'),
(606, 'Modasa'),
(607, 'Modinagar'),
(608, 'Moga'),
(609, 'Mohali'),
(610, 'Mokama'),
(611, 'Molarband'),
(612, 'Moradabad'),
(613, 'Morena'),
(614, 'Mormugao'),
(615, 'Morvi'),
(616, 'Motihari'),
(617, 'Mubarakpur'),
(618, 'Mughal Sarai'),
(619, 'Mumbai'),
(620, 'Mundka'),
(621, 'Munger'),
(622, 'Muradnagar'),
(623, 'Murwara'),
(624, 'Mustafabad'),
(625, 'Muzaffarnagar'),
(626, 'Muzaffarpur'),
(1010, 'myso'),
(1006, 'mysore'),
(627, 'Nadiad'),
(628, 'Nagaon'),
(629, 'Nagapattinam'),
(630, 'Nagaur'),
(631, 'Nagda'),
(632, 'Nagercoil'),
(633, 'Nagina'),
(634, 'Nagpur'),
(635, 'Naihati'),
(636, 'Najibabad'),
(637, 'Nalasopara'),
(638, 'Nalgonda'),
(639, 'Namakkal'),
(640, 'Nanded'),
(641, 'Nandurbar'),
(642, 'Nandyal'),
(643, 'Nangloi Jat'),
(644, 'Narasapur'),
(645, 'Narasaraopet'),
(646, 'Narnaul'),
(647, 'Narwana'),
(648, 'Nashik'),
(649, 'Navadwip'),
(650, 'Navagam Ghed'),
(651, 'Navghar'),
(652, 'Navi Mumbai'),
(653, 'Navsari'),
(654, 'Nawabganj'),
(655, 'Nawada'),
(656, 'Nawalgarh'),
(657, 'Nedumangad'),
(658, 'Nellur'),
(659, 'Nerkunram'),
(660, 'Neyveli'),
(661, 'Neyyattinkara'),
(662, 'Ni Barakpur'),
(663, 'Ni Dilli'),
(664, 'Nimach'),
(665, 'Nimbahera'),
(666, 'Nipani'),
(667, 'Nirmal'),
(668, 'Nizamabad'),
(669, 'Noida'),
(670, 'Nokha'),
(671, 'North Barakpur'),
(672, 'North Dum Dum'),
(673, 'Nuzvid'),
(674, 'Obra'),
(675, 'Old Maldah'),
(676, 'Ongole'),
(677, 'Orai'),
(678, 'Osmanabad'),
(1013, 'Others'),
(679, 'Ozhukarai'),
(680, 'Palakkad'),
(681, 'Palakollu'),
(682, 'Palasa'),
(683, 'Palghar'),
(684, 'Pali'),
(685, 'Palitana'),
(686, 'Pallavaram'),
(687, 'Pallichal'),
(688, 'Palwal'),
(689, 'Palwancha'),
(690, 'Pammal'),
(1, 'Pan-India'),
(691, 'Panaji'),
(692, 'Panchkula'),
(693, 'Pandharpur'),
(694, 'Panihati'),
(695, 'Panipat'),
(696, 'Pannuratti'),
(697, 'Paradwip'),
(698, 'Paramakkudi'),
(699, 'Parbhani'),
(700, 'Patan'),
(701, 'Patancheru'),
(702, 'Pathankot'),
(703, 'Patiala'),
(704, 'Patna'),
(705, 'Pattanagere'),
(706, 'Pattukkottai'),
(707, 'Payyannur'),
(708, 'Phagwara'),
(709, 'Phaltan'),
(710, 'Phulia'),
(711, 'Phulwari'),
(712, 'Phusro'),
(713, 'Piduguralla'),
(714, 'Pilibhit'),
(715, 'Pilkhuwa'),
(716, 'Pimpri'),
(717, 'Pithampur'),
(718, 'Pithoragarh'),
(719, 'Pollachi'),
(720, 'Pondicherry'),
(721, 'Ponnani'),
(722, 'Ponnur'),
(723, 'Porbandar'),
(724, 'Port Blair'),
(725, 'Proddatur'),
(726, 'Pudukkottai'),
(727, 'Pujali'),
(728, 'Pul Pehlad'),
(729, 'Puliyankudi'),
(730, 'Puna'),
(731, 'Punamalli'),
(733, 'Pune'),
(732, 'Pune Cantonment'),
(734, 'Puri'),
(735, 'Purnia'),
(736, 'Puruliya'),
(737, 'Pusad'),
(738, 'Puth Kalan'),
(739, 'Puttur'),
(740, 'Qutubullapur'),
(990, 'qwerty'),
(991, 'qwerty1'),
(741, 'Rabkavi'),
(742, 'Rae Bareli'),
(743, 'Raghogarh'),
(744, 'Raichur'),
(745, 'Raiganj'),
(746, 'Raigarh'),
(747, 'Raipur'),
(748, 'Rajamahendri'),
(749, 'Rajampet'),
(750, 'Rajapalaiyam'),
(751, 'Rajendranagar'),
(752, 'Rajkot'),
(753, 'Rajnandgaon'),
(754, 'Rajpur'),
(755, 'Rajpura'),
(756, 'Rajsamand'),
(757, 'Ramachandrapuram'),
(758, 'Ramagundam'),
(759, 'Ramanagaram'),
(760, 'Ramanathapuram'),
(762, 'Ramgarh'),
(761, 'Ramgarh Nagla Kothi'),
(763, 'Ramod'),
(765, 'Rampur'),
(764, 'Rampur Hat'),
(766, 'Ranaghat'),
(767, 'Ranchi'),
(768, 'Ranibennur'),
(769, 'Raniganj'),
(770, 'Ranip'),
(771, 'Ratangarh'),
(772, 'Rath'),
(773, 'Ratlam'),
(774, 'Ratnagiri'),
(776, 'Raurkela'),
(775, 'Raurkela Industrial Township'),
(777, 'Raxaul'),
(778, 'Rayachoti'),
(779, 'Rayadrug'),
(780, 'Rayagada'),
(781, 'Renukut'),
(782, 'Rewa'),
(783, 'Rewari'),
(784, 'Rishikesh'),
(785, 'Rishra'),
(786, 'Robertsonpet'),
(787, 'Rohtak'),
(788, 'Roshan Pura'),
(789, 'Rudrapur'),
(790, 'Rupnagar'),
(791, 'Rurki'),
(792, 'Sadat Pur Gujran'),
(793, 'Sagar'),
(794, 'Saharanpur'),
(795, 'Saharsa'),
(796, 'Sahaswan'),
(797, 'Sahibganj'),
(798, 'Salem'),
(799, 'Samalkot'),
(800, 'Samana'),
(801, 'Samastipur'),
(802, 'Sambalpur'),
(803, 'Sambhal'),
(804, 'Sandila'),
(805, 'Sangareddi'),
(806, 'SangliMiraj'),
(807, 'Sangrur'),
(808, 'Sankarankoil'),
(809, 'Sardarshahr'),
(810, 'Sarni'),
(811, 'Sasaram'),
(812, 'Satara'),
(813, 'Satna'),
(814, 'Sattenapalle'),
(815, 'Saunda'),
(816, 'Sawai Madhopur'),
(817, 'Sehore'),
(818, 'Sendhwa'),
(819, 'Seoni'),
(820, 'Serilungampalle'),
(821, 'Shahabad'),
(822, 'Shahada'),
(823, 'Shahdol'),
(824, 'Shahjahanpur'),
(825, 'Shahpur'),
(826, 'Shajapur'),
(827, 'Shamli'),
(828, 'Shantipur'),
(829, 'Shegaon'),
(830, 'Sheopur'),
(831, 'Sherkot'),
(832, 'Shikohabad'),
(833, 'Shiliguri'),
(834, 'Shillong'),
(835, 'Shimla'),
(836, 'Shimoga'),
(837, 'Shirpur'),
(838, 'Shivapuri'),
(839, 'Sholapur'),
(840, 'Shorapur'),
(841, 'Shrirampur'),
(842, 'Sibsagar'),
(843, 'Siddhapur'),
(844, 'Siddipet'),
(845, 'Sidhi'),
(846, 'Sidlaghatta'),
(847, 'Sihor'),
(848, 'Sikandarabad'),
(849, 'Sikar'),
(850, 'Silchar'),
(851, 'Sillod'),
(852, 'Sindari'),
(853, 'Singrauli'),
(855, 'Sirhind'),
(857, 'Sirsi'),
(858, 'Sirsilla'),
(859, 'Sitamarhi'),
(860, 'Sitapur'),
(861, 'Siuri'),
(862, 'Sivakasi'),
(863, 'Siwan'),
(864, 'Sonipat'),
(865, 'Sopur'),
(866, 'South Dum Dum'),
(867, 'Srikakulam'),
(868, 'Srikalahasti'),
(869, 'Srinagar'),
(870, 'Srivilliputtur'),
(1012, 'ss'),
(1004, 'su'),
(871, 'Sujangarh'),
(872, 'Sukhmalpur Nizamabad'),
(874, 'Sultanpur'),
(873, 'Sultanpur Majra'),
(875, 'Sunabeda'),
(876, 'Sunam'),
(877, 'Supaul'),
(878, 'Surat'),
(879, 'Suratgarh'),
(880, 'Surendranagar'),
(881, 'Suriapet'),
(882, 'Tadepalle'),
(883, 'Tadepallegudem'),
(884, 'Tadpatri'),
(885, 'Tajpul'),
(886, 'Talipparamba'),
(887, 'Tambaram'),
(888, 'Tanda'),
(889, 'Tandur'),
(890, 'Tanuku'),
(891, 'Tarn Taran'),
(892, 'Tenali'),
(893, 'Tenkasi'),
(894, 'Tezpur'),
(895, 'Thalassery'),
(896, 'Thaltej'),
(897, 'Thana'),
(898, 'Thanesar'),
(899, 'Thanjavur'),
(900, 'Theni Allinagaram'),
(901, 'Thiruthangal'),
(902, 'Thiruvananthapuram'),
(903, 'Thiruvarur'),
(904, 'Thrippunithura'),
(905, 'Thrissur'),
(906, 'Thuthukkudi'),
(907, 'Tigri'),
(908, 'Tikamgarh'),
(909, 'Tilhar'),
(910, 'Tindivanam'),
(911, 'Tinsukia'),
(912, 'Tiptur'),
(913, 'Tiruchchirappalli'),
(914, 'Tiruchengode'),
(915, 'Tirunelveli'),
(916, 'Tirupathur'),
(917, 'Tirupati'),
(918, 'Tiruppur'),
(919, 'Tirur'),
(920, 'Tiruvalla'),
(921, 'Tiruvannamalai'),
(922, 'Tiruvottiyur'),
(923, 'Titagarh'),
(924, 'Tohana'),
(926, 'Tumkur'),
(927, 'Tundla'),
(929, 'Tura'),
(930, 'Udagamandalam'),
(931, 'Udaipur'),
(932, 'Udgir'),
(933, 'Udhampur'),
(934, 'Udumalaipettai'),
(935, 'Udupi'),
(936, 'Ujhani'),
(937, 'Ujjain'),
(938, 'Ulhasnagar'),
(939, 'Ullal'),
(940, 'Ulubaria'),
(942, 'Unjha'),
(943, 'Unnao'),
(944, 'Upleta'),
(945, 'Uppal Kalan'),
(946, 'Uran Islampur'),
(947, 'UttarparaKotrung'),
(948, 'Vadakara'),
(949, 'Vadodara'),
(950, 'Valparai'),
(951, 'Valsad'),
(952, 'Vaniyambadi'),
(954, 'Varanasi'),
(956, 'Vastral'),
(957, 'Vejalpur'),
(958, 'Velampalaiyam'),
(959, 'Velluru'),
(960, 'Veraval'),
(961, 'Vidisha'),
(962, 'Vijalpor'),
(963, 'Vijayawada'),
(964, 'Viluppuram'),
(965, 'Vinukonda'),
(966, 'Virappanchatram'),
(968, 'Virudhachalam'),
(969, 'Virudunagar'),
(970, 'Visakhapatnam'),
(971, 'Visnagar'),
(972, 'Vizianagaram'),
(973, 'Vrindavan'),
(974, 'Vuyyuru'),
(975, 'Wadhwan'),
(976, 'Wadi'),
(978, 'Wanparti'),
(979, 'Warangal'),
(980, 'Wardha'),
(981, 'Warud'),
(982, 'Washim'),
(983, 'Wokha'),
(984, 'Yadgiri'),
(985, 'Yamunanagar'),
(986, 'Yavatmal'),
(987, 'Yelahanka'),
(988, 'Yemmiganur'),
(989, 'Ziauddin Pur');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `company_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `company_name`) VALUES
(32, 'Acc'),
(16, 'AFM'),
(13, 'Aior'),
(64, 'Airvoice'),
(49, 'Alexcosta'),
(5, 'AMF'),
(67, 'Anchor'),
(12, 'Appbee'),
(51, 'Apple'),
(26, 'Costafashion'),
(57, 'Dell'),
(29, 'Euro'),
(4, 'Google'),
(1, 'Infosys'),
(19, 'Jindal'),
(44, 'Kadee'),
(36, 'Kelvin'),
(55, 'LG'),
(66, 'LIC'),
(46, 'Luxum'),
(31, 'Mahindra'),
(39, 'MisbaIron'),
(63, 'Nsplus'),
(61, 'Oppo'),
(74, 'Others'),
(53, 'Panosonic'),
(28, 'Raybon'),
(41, 'Saifbrick'),
(50, 'Samsung'),
(30, 'Tata'),
(2, 'Tech Mahindra'),
(59, 'Vivo'),
(70, 'Wellness associates'),
(3, 'Wipro');

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `id` int(11) NOT NULL,
  `designation` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`id`, `designation`) VALUES
(1, 'HR'),
(3, 'Wellness manager'),
(4, 'Facility manager'),
(6, 'Employee'),
(7, 'Manager'),
(8, 'Chief Executive Officer');

-- --------------------------------------------------------

--
-- Table structure for table `engagement_master`
--

CREATE TABLE `engagement_master` (
  `id` int(11) NOT NULL,
  `engagement_type` varchar(200) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `engagement_master`
--

INSERT INTO `engagement_master` (`id`, `engagement_type`, `createdAt`, `ModifiedAt`) VALUES
(1, 'Physical Wellness', '2019-12-28 11:55:53', '2019-12-28 11:55:53'),
(2, 'Nutrition Wellness', '2019-12-28 11:56:09', '2019-12-28 11:56:09'),
(3, 'Spiritual Wellness', '2019-12-28 11:56:25', '2019-12-28 11:56:25'),
(4, 'Emotional Wellness', '2019-12-28 11:56:39', '2019-12-28 11:56:39'),
(5, 'Mental Wellness', '2019-12-28 11:57:12', '2019-12-28 11:57:12'),
(6, 'Social Wellness', '2019-12-28 11:57:23', '2019-12-28 11:57:23'),
(7, 'Festive workshop and events\r\n', '2021-04-01 17:04:56', '2021-04-01 17:04:56');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `companyId` bigint(10) NOT NULL,
  `cityId` bigint(10) NOT NULL,
  `locationId` bigint(10) NOT NULL,
  `facilitiesManaged` bigint(10) NOT NULL,
  `workshops` varchar(255) NOT NULL,
  `happyUsers` varchar(255) NOT NULL,
  `clients` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `companyId`, `cityId`, `locationId`, `facilitiesManaged`, `workshops`, `happyUsers`, `clients`) VALUES
(1, 12, 857, 38, 150, '4123', '100000', '500'),
(2, 16, 1013, 32, 1, '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `final_services`
--

CREATE TABLE `final_services` (
  `id` int(11) NOT NULL,
  `service_id` bigint(10) NOT NULL,
  `frequency_id` bigint(10) NOT NULL,
  `engament_id` bigint(10) NOT NULL,
  `leval1_cat` bigint(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `final_services`
--

INSERT INTO `final_services` (`id`, `service_id`, `frequency_id`, `engament_id`, `leval1_cat`, `created_at`) VALUES
(1, 1, 1, 1, 1, '2020-01-29 11:01:34'),
(3, 1, 3, 1, 1, '2020-01-31 10:48:56'),
(5, 2, 1, 1, 1, '2020-01-31 10:53:22'),
(6, 2, 3, 1, 1, '2020-01-31 10:53:22'),
(7, 3, 1, 2, 1, '2020-01-31 11:28:58'),
(8, 3, 3, 2, 1, '2020-01-31 11:28:58'),
(9, 4, 1, 1, 1, '2020-01-31 11:40:01'),
(10, 4, 3, 1, 1, '2020-01-31 11:40:01'),
(11, 5, 1, 3, 1, '2020-01-31 11:42:09'),
(12, 5, 3, 3, 2, '2020-01-31 11:42:09'),
(13, 6, 1, 1, 1, '2020-01-31 11:43:42'),
(14, 6, 3, 1, 2, '2020-01-31 11:43:42'),
(15, 7, 1, 1, 2, '2020-01-31 11:45:05'),
(16, 7, 3, 1, 3, '2020-01-31 11:45:05'),
(17, 8, 1, 3, 2, '2020-01-31 11:46:27'),
(18, 8, 3, 3, 2, '2020-01-31 11:46:27'),
(19, 9, 1, 3, 4, '2020-01-31 11:50:07'),
(20, 9, 3, 3, 1, '2020-01-31 11:50:07'),
(21, 10, 1, 3, 5, '2020-01-31 11:52:32'),
(22, 10, 3, 3, 2, '2020-01-31 11:52:32'),
(23, 11, 1, 3, 2, '2020-01-31 11:55:28'),
(24, 11, 3, 3, 3, '2020-01-31 11:55:28'),
(25, 12, 1, 5, 3, '2020-01-31 11:58:54'),
(26, 12, 3, 5, 4, '2020-01-31 11:58:54'),
(27, 13, 1, 1, 5, '2020-01-31 12:01:22'),
(28, 13, 3, 1, 5, '2020-01-31 12:01:22'),
(29, 14, 1, 6, 4, '2020-01-31 12:02:21'),
(30, 14, 3, 6, 4, '2020-01-31 12:02:21'),
(31, 17, 1, 1, 3, '2020-01-31 12:03:46'),
(32, 17, 3, 1, 3, '2020-01-31 12:03:46'),
(36, 18, 1, 5, 3, '2020-01-31 12:27:29'),
(37, 18, 3, 5, 4, '2020-01-31 12:27:29'),
(38, 19, 1, 5, 2, '2020-01-31 12:29:42'),
(39, 19, 3, 5, 5, '2020-01-31 12:29:42'),
(40, 20, 1, 5, 5, '2020-01-31 12:31:02'),
(41, 20, 3, 5, 5, '2020-01-31 12:31:02'),
(46, 22, 1, 2, 5, '2020-01-31 12:42:18'),
(47, 22, 3, 2, 5, '2020-01-31 12:42:18'),
(48, 21, 1, 6, 2, '2020-01-31 12:44:57'),
(49, 21, 3, 6, 2, '2020-01-31 12:44:57'),
(50, 23, 1, 5, 3, '2020-01-31 12:46:08'),
(51, 23, 3, 5, 2, '2020-01-31 12:46:08'),
(52, 25, 1, 5, 2, '2020-01-31 15:27:50'),
(53, 25, 3, 5, 2, '2020-01-31 15:27:50'),
(54, 26, 1, 5, 2, '2020-01-31 15:31:19'),
(55, 26, 3, 5, 2, '2020-01-31 15:31:19'),
(56, 27, 1, 5, 3, '2020-01-31 15:31:59'),
(57, 27, 3, 5, 3, '2020-01-31 15:31:59'),
(58, 28, 1, 1, 3, '2020-01-31 15:33:18'),
(59, 28, 3, 1, 1, '2020-01-31 15:33:18'),
(60, 29, 1, 1, 2, '2020-01-31 15:35:26'),
(61, 29, 3, 1, 5, '2020-01-31 15:35:26'),
(62, 30, 1, 1, 5, '2020-01-31 15:39:02'),
(63, 30, 3, 1, 3, '2020-01-31 15:39:02'),
(64, 31, 1, 1, 4, '2020-01-31 15:55:19'),
(65, 31, 3, 1, 3, '2020-01-31 15:55:19'),
(66, 32, 1, 1, 5, '2020-01-31 15:55:45'),
(67, 32, 3, 1, 2, '2020-01-31 15:55:45'),
(68, 33, 1, 1, 1, '2020-01-31 15:56:45'),
(69, 5, 1, 5, 2, '2020-01-31 18:25:06'),
(70, 5, 3, 5, 5, '2020-01-31 18:25:06'),
(71, 10, 1, 1, 2, '2020-01-31 18:29:39'),
(72, 10, 3, 1, 1, '2020-01-31 18:29:39'),
(73, 11, 1, 5, 2, '2020-01-31 18:38:05'),
(74, 11, 3, 5, 3, '2020-01-31 18:38:05'),
(75, 13, 1, 5, 3, '2020-01-31 18:39:57'),
(76, 13, 3, 5, 3, '2020-01-31 18:39:57'),
(77, 17, 1, 6, 2, '2020-01-31 18:41:45'),
(78, 17, 3, 6, 4, '2020-01-31 18:41:45'),
(79, 18, 1, 4, 3, '2020-01-31 18:44:39'),
(80, 18, 3, 4, 2, '2020-01-31 18:44:39'),
(81, 19, 1, 3, 5, '2020-01-31 18:46:09'),
(82, 19, 3, 3, 1, '2020-01-31 18:46:09'),
(83, 20, 1, 2, 4, '2020-01-31 18:53:57'),
(84, 20, 3, 2, 2, '2020-01-31 18:53:57'),
(85, 20, 1, 6, 2, '2020-01-31 18:54:21'),
(86, 20, 3, 6, 4, '2020-01-31 18:54:21'),
(87, 21, 1, 1, 3, '2020-01-31 18:56:35'),
(88, 21, 1, 1, 1, '2020-01-31 18:56:35'),
(89, 23, 1, 6, 2, '2020-01-31 18:59:12'),
(90, 23, 3, 6, 4, '2020-01-31 18:59:12'),
(93, 25, 1, 2, 5, '2020-02-01 11:46:40'),
(94, 25, 3, 2, 3, '2020-02-01 11:46:40'),
(95, 26, 1, 3, 4, '2020-02-01 11:47:34'),
(96, 26, 3, 3, 3, '2020-02-01 11:47:34'),
(97, 27, 1, 3, 2, '2020-02-01 11:48:01'),
(98, 27, 3, 3, 1, '2020-02-01 11:48:01'),
(99, 28, 1, 5, 5, '2020-02-01 11:48:49'),
(100, 28, 3, 5, 4, '2020-02-01 11:48:49'),
(101, 32, 1, 5, 3, '2020-02-01 11:49:59'),
(102, 32, 3, 5, 2, '2020-02-01 11:49:59'),
(103, 33, 1, 1, 1, '2020-02-01 11:52:27'),
(104, 33, 3, 3, 5, '2020-02-01 11:52:27'),
(105, 200, 200, 3000, 700, '2020-06-17 15:44:51'),
(112, 42, 1, 1, 1, '2020-12-02 16:42:53'),
(113, 42, 2, 1, 1, '2020-12-02 16:42:53'),
(114, 42, 1, 3, 1, '2020-12-02 16:42:53'),
(115, 42, 2, 3, 1, '2020-12-02 16:42:53'),
(116, 42, 1, 4, 1, '2020-12-02 16:42:53'),
(117, 42, 2, 4, 1, '2020-12-02 16:42:53'),
(124, 43, 1, 1, 1, '2020-12-02 17:50:23'),
(125, 43, 2, 1, 1, '2020-12-02 17:50:23'),
(126, 43, 1, 3, 1, '2020-12-02 17:50:23'),
(127, 43, 2, 3, 1, '2020-12-02 17:50:23'),
(128, 43, 1, 4, 1, '2020-12-02 17:50:24'),
(129, 43, 2, 4, 1, '2020-12-02 17:50:24'),
(130, 44, 0, 0, 0, '2020-12-03 11:41:25');

-- --------------------------------------------------------

--
-- Table structure for table `frequency_master`
--

CREATE TABLE `frequency_master` (
  `id` int(11) NOT NULL,
  `frequent_range` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `frequency_master`
--

INSERT INTO `frequency_master` (`id`, `frequent_range`) VALUES
(4, 'Annually'),
(2, 'Bimonthly'),
(1, 'Monthly'),
(3, 'Quarterly');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_list`
--

CREATE TABLE `gallery_list` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gallery_list`
--

INSERT INTO `gallery_list` (`id`, `title`, `timeStamp`) VALUES
(56, 'Desk Yoga at FCA, Chennai, Sept 2018', '2021-04-05 04:57:37'),
(57, 'Desk Yoga at FCA, Mumbai, Sept 2018', '2021-04-05 04:57:44'),
(58, 'Food for life at UPL, Mumbai, Sep 2016', '2021-04-05 04:57:58'),
(59, 'Kick Boxing & Boot Camp at ZS associates India Pvt ltd, Pune, Sept & Oct 2018', '2021-04-05 04:58:09'),
(60, 'Wellness Activities at J & J, Gurgaon', '2021-04-05 04:58:15'),
(61, 'Womens day Celebration Workshop, Aditya Birla Finance, Mumbai', '2021-04-05 04:58:32'),
(66, 'World health day at HUL, Mumbai', '2021-04-05 04:58:38');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `image_text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image`, `image_text`) VALUES
(9, 'microgreens-wellness-association.jpg', 'eee'),
(10, '04.jpg', '22'),
(11, '08.jpg', 'image'),
(12, 'soulbowlhealing-wellness_association.jpg', 'suoalbowal'),
(13, 'Setup.exe', 'xx'),
(14, 'Default-workshop-Image.png', 'ss'),
(15, '06.jpg', 'd'),
(16, '07.jpg', 'c'),
(17, '05.jpg', 'ee'),
(18, 'yoga-11wellness_association.jpg', 'rtyuu'),
(19, 'business.jpg', 'vvv'),
(20, '06.jpg', 'ss'),
(21, '06.jpg', 'qqq'),
(22, 'carpel-tunnel-syndrome-wellness-assocition.jpg', 'det'),
(23, '', 'wert'),
(24, '06.jpg', 're'),
(25, '', ''),
(26, '', 'e'),
(27, '', 'tt'),
(28, '', 'er'),
(29, 'backtoback-wellness-association.jpg', 'etertet'),
(30, '04.jpg', '01'),
(31, '07.jpg', '02'),
(32, 'vishl.jpg', ''),
(33, 'vishl.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `image_list`
--

CREATE TABLE `image_list` (
  `imageId` int(11) NOT NULL,
  `imagePath` text NOT NULL,
  `galleryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `image_list`
--

INSERT INTO `image_list` (`imageId`, `imagePath`, `galleryId`) VALUES
(165, '/galleryImages/wellness-desk-yoga-at-fca-chennai-sept-2018-0.94908000-1616046912.jpg', 56),
(166, '/galleryImages/wellness-desk-yoga-at-fca-chennai-sept-2018-0.95028900-1616046912.jpg', 56),
(167, '/galleryImages/wellness-desk-yoga-at-fca-chennai-sept-2018-0.95120200-1616046912.jpg', 56),
(168, '/galleryImages/wellness-desk-yoga-at-fca-chennai-sept-2018-0.95199600-1616046912.jpg', 56),
(169, '/galleryImages/wellness-desk-yoga-at-fca-chennai-sept-2018-0.95283500-1616046912.jpg', 56),
(170, '/galleryImages/wellness-desk-yoga-at-fca-chennai-sept-2018-0.95365200-1616046912.jpg', 56),
(171, '/galleryImages/wellness-desk-yoga-at-fca-chennai-sept-2018-0.95447600-1616046912.jpg', 56),
(172, '/galleryImages/wellness-desk-yoga-at-fca-chennai-sept-2018-0.95530400-1616046912.jpg', 56),
(173, '/galleryImages/wellness-desk-yoga-at-fca-mumbai-sept-2018-0.77794200-1616046991.jpg', 57),
(174, '/galleryImages/wellness-desk-yoga-at-fca-mumbai-sept-2018-0.77917800-1616046991.jpg', 57),
(175, '/galleryImages/wellness-desk-yoga-at-fca-mumbai-sept-2018-0.78011600-1616046991.jpg', 57),
(176, '/galleryImages/wellness-desk-yoga-at-fca-mumbai-sept-2018-0.78100500-1616046991.jpg', 57),
(177, '/galleryImages/wellness-desk-yoga-at-fca-mumbai-sept-2018-0.78188500-1616046991.jpg', 57),
(178, '/galleryImages/wellness-desk-yoga-at-fca-mumbai-sept-2018-0.78276300-1616046991.jpg', 57),
(179, '/galleryImages/wellness-desk-yoga-at-fca-mumbai-sept-2018-0.78362600-1616046991.jpg', 57),
(180, '/galleryImages/wellness-desk-yoga-at-fca-mumbai-sept-2018-0.78450500-1616046991.jpg', 57),
(181, '/galleryImages/wellness-food-for-life-at-upl-mumbai-sep-2016-0.50278900-1616047019.jpg', 58),
(182, '/galleryImages/wellness-food-for-life-at-upl-mumbai-sep-2016-0.50432700-1616047019.jpg', 58),
(183, '/galleryImages/wellness-food-for-life-at-upl-mumbai-sep-2016-0.50543400-1616047019.jpg', 58),
(184, '/galleryImages/wellness-food-for-life-at-upl-mumbai-sep-2016-0.50653400-1616047019.jpg', 58),
(185, '/galleryImages/wellness-kick-boxing-boot-camp-at-zs-associates-india-pvt-ltd-pune-sept-oct-2018-0.55421600-1616047089.jpg', 59),
(186, '/galleryImages/wellness-kick-boxing-boot-camp-at-zs-associates-india-pvt-ltd-pune-sept-oct-2018-0.55546100-1616047089.jpg', 59),
(187, '/galleryImages/wellness-kick-boxing-boot-camp-at-zs-associates-india-pvt-ltd-pune-sept-oct-2018-0.55637100-1616047089.jpg', 59),
(188, '/galleryImages/wellness-kick-boxing-boot-camp-at-zs-associates-india-pvt-ltd-pune-sept-oct-2018-0.55726100-1616047089.jpg', 59),
(189, '/galleryImages/wellness-kick-boxing-boot-camp-at-zs-associates-india-pvt-ltd-pune-sept-oct-2018-0.55811700-1616047089.jpg', 59),
(190, '/galleryImages/wellness-kick-boxing-boot-camp-at-zs-associates-india-pvt-ltd-pune-sept-oct-2018-0.55901000-1616047089.jpg', 59),
(191, '/galleryImages/wellness-kick-boxing-boot-camp-at-zs-associates-india-pvt-ltd-pune-sept-oct-2018-0.55990200-1616047089.jpg', 59),
(192, '/galleryImages/wellness-kick-boxing-boot-camp-at-zs-associates-india-pvt-ltd-pune-sept-oct-2018-0.56078800-1616047089.jpg', 59),
(193, '/galleryImages/wellness-wellness-activities-at-j-j-gurgaon-0.21041200-1616047129.jpg', 60),
(194, '/galleryImages/wellness-wellness-activities-at-j-j-gurgaon-0.21177000-1616047129.jpg', 60),
(195, '/galleryImages/wellness-wellness-activities-at-j-j-gurgaon-0.21245500-1616047129.jpg', 60),
(196, '/galleryImages/wellness-wellness-activities-at-j-j-gurgaon-0.21311400-1616047129.jpg', 60),
(197, '/galleryImages/wellness-womens-day-celebration-workshop-aditya-birla-finance-mumbai-0.64141900-1616047735.jpg', 61),
(198, '/galleryImages/wellness-womens-day-celebration-workshop-aditya-birla-finance-mumbai-0.64263000-1616047735.jpg', 61),
(199, '/galleryImages/wellness-womens-day-celebration-workshop-aditya-birla-finance-mumbai-0.64358800-1616047735.jpg', 61),
(200, '/galleryImages/wellness-womens-day-celebration-workshop-aditya-birla-finance-mumbai-0.64448900-1616047735.jpg', 61),
(201, '/galleryImages/wellness-womens-day-celebration-workshop-aditya-birla-finance-mumbai-0.64537000-1616047735.jpg', 61),
(214, '/galleryImages/wellness-world-health-day-at-hul-mumbai-0.81765900-1616407827.30231400-1616047807', 66),
(215, '/galleryImages/wellness-world-health-day-at-hul-mumbai-0.81847100-1616407827.30295700-1616047807', 66),
(216, '/galleryImages/wellness-world-health-day-at-hul-mumbai-0.81908000-1616407827.30350700-1616047807', 66),
(217, '/galleryImages/wellness-world-health-day-at-hul-mumbai-0.81965700-1616407827.30402400-1616047807', 66);

-- --------------------------------------------------------

--
-- Table structure for table `image_thumbnail`
--

CREATE TABLE `image_thumbnail` (
  `id` int(11) NOT NULL,
  `thumbnail` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `image_thumbnail`
--

INSERT INTO `image_thumbnail` (`id`, `thumbnail`) VALUES
(8, '1825.jpg'),
(9, '38.jpg'),
(10, ''),
(11, ''),
(12, 'aa'),
(13, '\r\n\r\n44'),
(14, 'noimage'),
(15, 'jjuu'),
(16, 'eee'),
(17, 'ff'),
(18, 'gg'),
(19, ''),
(20, 'rtyuu'),
(21, 'd'),
(22, '22'),
(23, '22'),
(24, 'eee'),
(25, '444'),
(26, 'eee'),
(27, 'gtt'),
(28, ''),
(29, 'gtt'),
(30, ''),
(31, ''),
(32, ''),
(33, ''),
(34, ''),
(35, ''),
(36, 'ff'),
(37, ''),
(38, 'ff'),
(39, ''),
(40, 'ereyy'),
(41, '01');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `leadId` int(11) NOT NULL,
  `userid` bigint(10) NOT NULL,
  `lead_status` varchar(50) DEFAULT 'Created',
  `service` text NOT NULL,
  `contactNumber` text NOT NULL,
  `lead_date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`leadId`, `userid`, `lead_status`, `service`, `contactNumber`, `lead_date`) VALUES
(1, 5, 'Deleted', 'general', 'hÉôDYºÍ‰¤Ñ?Ö¢š', '2021-03-24 13:18:22'),
(2, 5, 'Deleted', 'general', 'hÉôDYºÍ‰¤Ñ?Ö¢š', '24-03-2021 01:20:27 PM'),
(3, 5, 'Deleted', 'general', 'hÉôDYºÍ‰¤Ñ?Ö¢š', '01-04-2021 05:39:36 PM'),
(4, 5, 'Deleted', 'general', 'hÉôDYºÍ‰¤Ñ?Ö¢š', '02-04-2021 05:43:05 PM'),
(5, 5, 'Deleted', 'general', 'hÉôDYºÍ‰¤Ñ?Ö¢š', '02-04-2021 05:43:10 PM'),
(6, 21, 'Deleted', 'general', '×ŠB\'M‡ÙÞZãÆÝj=­', '29-04-2021 10:33:22 AM'),
(7, 21, 'Deleted', 'general', '×ŠB\'M‡ÙÞZãÆÝj=­', '29-04-2021 10:33:25 AM'),
(8, 5, 'Created', 'general', '×ŠB\'M‡ÙÞZãÆÝj=­', '29-04-2021 06:46:45 PM'),
(9, 35, 'Deleted', 'general', '­¶Éwà—9Ê\'ÐMx¬', '29-04-2021 07:44:52 PM'),
(10, 5, 'Created', 'Yoga', '×ŠB\'M‡ÙÞZãÆÝj=­', '01-05-2021 11:17:07 AM'),
(11, 5, 'Created', 'general', '×ŠB\'M‡ÙÞZãÆÝj=­', '01-05-2021 11:17:51 AM'),
(12, 37, 'Created', 'Kick Boxing', '=0ªT”ø-Â™¾~<dYÈˆ', '11-05-2021 04:29:28 PM'),
(13, 37, 'Created', 'Tai Chi', '=0ªT”ø-Â™¾~<dYÈˆ', '11-05-2021 04:37:31 PM'),
(14, 38, 'Created', 'Food for life', 'ÃâGÁ¼\'}ÑëýôÝF]Ôº', '11-05-2021 04:47:38 PM');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `location_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `location_name`) VALUES
(18, 'Ajmer'),
(29, 'Andheri'),
(6, 'Ankleshwar'),
(27, 'Bandra'),
(14, 'Dharwad'),
(26, 'Electronic city'),
(37, 'Hubli'),
(35, 'Indiranagar'),
(32, 'Manyata tech park'),
(31, 'Marine drive'),
(19, 'Mysore'),
(22, 'Mysore-first cross'),
(38, 'Others'),
(17, 'UttarKanda'),
(30, 'West Mumbai'),
(28, 'Whitefield');

-- --------------------------------------------------------

--
-- Table structure for table `member_master`
--

CREATE TABLE `member_master` (
  `id` int(11) NOT NULL,
  `members` varchar(100) NOT NULL,
  `min_batch` varchar(100) NOT NULL,
  `max_batch` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_master`
--

INSERT INTO `member_master` (`id`, `members`, `min_batch`, `max_batch`, `createdAt`, `modifiedAt`) VALUES
(1, 'Less Than 20', '15', '30', '2019-12-28 11:44:09', '2019-12-28 11:44:09'),
(2, '20 to 51', '20', '35', '2019-12-28 11:44:26', '2019-12-28 11:44:26'),
(3, '50 to 100', '25', '50', '2019-12-28 11:44:49', '2019-12-28 11:44:49'),
(4, '100 to 200', '30', '60', '2019-12-28 11:45:03', '2019-12-28 11:45:03');

-- --------------------------------------------------------

--
-- Table structure for table `r_frequancy`
--

CREATE TABLE `r_frequancy` (
  `id` int(11) NOT NULL,
  `service` bigint(10) NOT NULL,
  `engagment_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `r_frequancy`
--

INSERT INTO `r_frequancy` (`id`, `service`, `engagment_id`) VALUES
(20, 1, 1),
(21, 2, 1),
(22, 2, 2),
(23, 9, 1),
(24, 9, 2),
(25, 9, 1),
(26, 9, 3),
(27, 10, 1),
(28, 10, 5),
(29, 11, 1),
(30, 11, 2),
(31, 11, 3),
(32, 11, 4),
(33, 11, 5);

-- --------------------------------------------------------

--
-- Table structure for table `selected_services`
--

CREATE TABLE `selected_services` (
  `id` int(11) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `product_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `selected_services`
--

INSERT INTO `selected_services` (`id`, `user_id`, `product_id`) VALUES
(1, 1, 22),
(2, 1, 27),
(3, 63, 33);

-- --------------------------------------------------------

--
-- Table structure for table `services_master`
--

CREATE TABLE `services_master` (
  `product_id` int(11) NOT NULL,
  `workshops` varchar(255) DEFAULT NULL,
  `description` text,
  `small_description` varchar(500) DEFAULT NULL,
  `images` varchar(300) DEFAULT NULL,
  `serviceBy` varchar(200) DEFAULT NULL,
  `full_image` varchar(300) DEFAULT NULL,
  `full_image_alt` varchar(100) DEFAULT NULL,
  `o_min_range` int(255) DEFAULT NULL,
  `o_max_range` int(11) DEFAULT NULL,
  `o_min_batch` varchar(255) DEFAULT NULL,
  `o_max_batch` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `v_min_range` int(11) DEFAULT NULL,
  `v_max_range` int(11) DEFAULT NULL,
  `v_min_batch` varchar(255) DEFAULT NULL,
  `v_max_batch` varchar(255) DEFAULT NULL,
  `location` varchar(100) NOT NULL,
  `age_group` varchar(255) DEFAULT NULL,
  `frequency_id` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `engagement_id` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `level1_cat` int(11) DEFAULT NULL,
  `html_page` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services_master`
--

INSERT INTO `services_master` (`product_id`, `workshops`, `description`, `small_description`, `images`, `serviceBy`, `full_image`, `full_image_alt`, `o_min_range`, `o_max_range`, `o_min_batch`, `o_max_batch`, `duration`, `v_min_range`, `v_max_range`, `v_min_batch`, `v_max_batch`, `location`, `age_group`, `frequency_id`, `engagement_id`, `level1_cat`, `html_page`, `created_at`) VALUES
(7, 'Self Defence', 'One can never go wrong with a self defence class. Empower yourself, take control of yourself and your narrative. And needless to say, self defence conditioning supports you in harnessing your strength and fitness abilities.', 'Be your own hero by learning simple techniques of self defence. In the everyday situation , self defence can empower you & make a big', '/image/wellness-self-defence-0.59780600-1616043840.jpg', 'Shreyash', 'self-defence-wellness-associations.jpg', '', 20000, 0, 'Upto 25', '50', '120 mins.', 20000, 0, 'Open', '0', '[1]', '20-40', '[1]', '[1,4,5]', 1, 'wellness-self-defence.html', '2020-01-29 12:40:07'),
(29, 'Aerobics', 'Aerobic exercise provides cardiovascular conditioning. The term aerobic actually means with oxygen, which means that breathing controls the amount of oxygen that can make it to the muscles to help them burn fuel and move.', 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown', '/image/wellness-aerobics-0.01952400-1616045507.jpg', 'Support', 'aerobics-wellness-association.jpg', '', 4000, 0, 'Upto 30', '50', '60 mins.', 4000, 0, 'Open', '0', '[1]', '20-60', '[1]', '[4,5,6,3]', 2, 'wellness-aerobics.html', '2020-01-30 18:56:20'),
(30, 'Kick Boxing', 'A cardio kickboxing class challenges your technique, endurance, and above all, concentration. Half the battle is mental â€” you need to focus on the individual movements that make up a combination.', 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown', '/image/wellness-kick-boxing-0.07225400-1616045624.jpg', 'Support', 'kickboxing-wellness_association.jpg', '', 5000, 0, 'Upto 30', '50', '60 mins.', 5000, 0, 'Open', '4', '[1]', '25-60', '[1]', '[3,4,5,6]', 2, 'wellness-kick-boxing.html', '2020-01-30 18:58:46'),
(175, 'Eye Yoga', 'You need coffee, and they need a break too. Relieve your eyes from the stress of staring at screens all day, and strengthen them for the remaining inevitable long hours with your laptop.', '', '/image/wellness-eye-yoga-0.49668500-1616043886.jpg', 'Support', '', '', 7000, 15000, '25-50', '50', '60 mins', 7000, 15000, 'Open', '50', '[1]', '20-60', '[1]', '[1]', 1, 'wellness-eye-yoga.html', '2021-01-12 11:56:24'),
(176, 'Back to Back', 'Stressed necks and backs can be a dangerous distraction. Get rid of cubicle fever through these practical demonstrations of movements and postures to ease out back pain and stiff necks.', '', '/image/wellness-back-to-back-0.70489200-1616044855.jpg', 'Support', '', '', 7000, 15000, '25-50', '50', '90 mins.', 7000, 15000, 'Open', '50', '[1]', '20-40', '[1]', '[1]', 1, 'wellness-back-to-back.html', '2021-01-12 11:56:36'),
(177, 'Food for life', 'Myth 1: Eating healthy is complicated.  Myth 2: Eating healthy is boring.  Find out simple ways to incorporate interesting healthy food habits for greater quality of daily life.', '', '/image/wellness-food-for-life-0.40110600-1616044907.jpg', 'Support', '', '', 7000, 15000, '25-50', '50', '90 mins.', 7000, 15000, 'Open', '50', '[1]', '20-40', '[1]', '[2]', 1, 'wellness-food-for-life.html', '2021-01-12 11:59:28'),
(178, 'Sleep Well', 'Deep sleep is essential for optimal focus and productivity. Learn ways to deepen a quality nightâ€™s sleep to ensure energy for tomorrowâ€™s work day.', '', '/image/wellness-sleep-well-0.08205300-1616044945.jpg', 'Support', '', '', 7000, 15000, '25-50', '50', '90 mins.', 7000, 15000, 'Open', '50', '[1]', '20-40', '[1]', '[4,5]', 1, 'wellness-sleep-well.html', '2021-01-12 12:03:31'),
(179, 'Mudra Yoga', 'You only need yourself to heal. Learn simple hand gestures to guide the right energies for remedying any sort of illness or pain. You wonâ€™t be frequenting sick days after this workshop. ', '', '/image/wellness-mudra-yoga-0.15163200-1616044994.jpg', 'Support', '', '', 7000, 15000, '25-50', '50', '60 mins', 7000, 15000, 'Open ', '50', '[1]', '20-40', '[1]', '[3,4,5]', 1, 'wellness-mudra-yoga.html', '2021-01-12 12:08:14'),
(281, 'Fit 15', '15 minutes, and a space the size of your desk - all you need to get your workout for the day sweated out in no time!', '', '/image/wellness-fit-15-0.70075300-1620906751.jpg', 'Support', NULL, '', 7000, 15000, '25-50', '', '90 mins.', 7000, 15000, 'Open', '', '[1]', '', '[1]', '[1]', 1, 'wellness-fit-15.html', '2021-01-25 17:52:42'),
(282, 'Yogic Way', 'While trends like SoulCycle and CrossFit come and go, Yoga has stood the test of time. And for right measure. Learn some basic yoga practices for effective concentration, ease, and flexibility amongst employees.', '', '/image/wellness-yogic-way-0.15378400-1616045317.jpg', 'Support', '', '', 7000, 15000, '25-50', '', '90 mins.', 7000, 15000, 'Open', '', '[1]', '', '[1]', '[6,5,4]', 1, 'wellness-yogic-way.html', '2021-01-25 17:53:40'),
(283, 'Zumba', 'One of the first thing about Zumba is that the motto is literally â€œDitch the workoutâ€”Join the party!â€ And yeah, that might sound like its not taking fitness super seriously, but donâ€™t misunderstand itâ€”Zumba is actually an incredible workout if youâ€™re doing it right and it can provide an enormous amount of added benefits to you.', '', '/image/wellness-zumba-0.57928500-1616045660.jpg', 'Support', '', '', 5000, 15000, '30-100', '', '60 mins', 5000, 15000, 'Open', '', '[1]', '', '[1]', '[4,3,5,6]', 2, 'wellness-zumba.html', '2021-01-25 17:55:21'),
(284, 'Mumbai Bhangra', 'One can Burn up to 500 calories in a single session and is a team oriented workout for your employees, has various health benefits. The famous moves of shoulder shrugging , toe tapping and head shaking to soaring and contagious beats of dhol brings out a fun way to lose weight.', '', '/image/wellness-mumbai-bhangra-0.47815500-1616045738.jpg', 'Support', '', '', 5000, 25000, 'Upto 30', '', '60 mins.', 5000, 25000, 'Open', '', '[1]', '', '[1]', '[3,4,5,6]', 1, 'wellness-mumbai-bhangra.html', '2021-01-25 17:56:40'),
(286, 'Yoga', 'The word â€œYogaâ€ essentially means, â€œthat which brings you to reality.â€ Yoga means to move towards an experiential reality where one knows the ultimate nature of the existence, the way it is made.\n', '', '/image/wellness-yoga-0.02793200-1616045779.jpg', 'Support', '', '', 4000, 0, 'Upto 30', '', '60 mins', 4000, 0, 'Open', '', '[1]', '', '[1]', '[6,5,4,3]', 2, 'wellness-yoga.html', '2021-01-25 17:58:07'),
(314, 'Tai Chi', 'There are many styles and forms of tai chi, the major ones being Chen, Yang, Wu, Wu (different words in Chinese) and Sun. Each style has its own features, but all styles share the same essential principles.', '', '/image/wellness-tai-chi-0.72200600-1616045817.jpg', 'Support', '', '', 5000, 7000, 'Upto 30', '', '60 mins', 5000, 7000, 'Open', '', '[1]', '', '[1]', '[3,4,5,6]', 2, 'wellness-tai-chi.html', '2021-01-28 16:02:25'),
(315, 'Pilates', 'Pilates classes can be slow paced and as you get stronger and gain experience they can be extremely tough and fast paced. The benefits of Pilates can be appreciated by all.', NULL, '/image/wellness-pillates-0.48401100-1616407387.jpg', 'Support', NULL, NULL, 5000, 0, 'Upto 30', NULL, '60 mins', 5000, 0, 'Open', NULL, '[1]', NULL, '[1]', '[5,4,3,6]', 2, 'wellness-pilates.html', '2021-03-22 03:01:54'),
(316, 'Free Hand Exercise', 'Freehand exercises have a tonic effect on the muscles and internal organs. They tone up the circulatory system and are beneficial in safeguarding the general health of the body. Advanced freehand exercises shape and muscularize the body.', NULL, '/image/wellness-free-hand-exercises-0.35502900-1616667336.jpg', 'Support', NULL, NULL, 4000, 0, 'Upto 30', NULL, '60mins', 4000, 0, 'open', NULL, '[1]', NULL, '[1]', '[5,4,3,6]', 2, 'wellness-free-hand-exercise.html', '2021-03-25 03:15:36'),
(317, 'Tabata', 'Tabata is a high-intensity interval training that consists of eight sets of fast-paced exercises each performed for 20 seconds interspersed with a brief rest of 10 seconds.', NULL, '/image/wellness-tabata-0.80294100-1616667497.jpg', 'Support', NULL, NULL, 4000, 0, 'Upto 30', NULL, '60 mins', 4000, 0, 'open', NULL, '[1]', NULL, '[1]', '[5,4,3,6]', 2, 'wellness-tabata.html', '2021-03-25 03:18:17'),
(318, 'Stress Buster Music', 'What better way to spend your break than learning the ropes, or rather, notes, of an instrument? An immediate stress buster guaranteed to send you back to your computer with an uplifted mood.', NULL, '/image/wellness-stress-buster-music-guitar-keyboard-0.11512700-1616667720.jpg', 'Support', NULL, NULL, 6500, 0, '12 sessions (Per person cost)', NULL, '60 mins (12 sessions)', 6500, 0, '12 sessions (Per person cost)', NULL, '[1]', NULL, '[1]', '[5,4,3,6]', 2, 'wellness-stress-buster-music.html', '2021-03-25 03:22:00'),
(319, 'Stretching Fun', 'Your brain reacts to how your body feels. Foster easy mobility of the muscles for better reception, invention, and calmness in oneâ€™s state of mind through a thorough stretching routine.', NULL, '/image/wellness-stretching-fun-0.50056400-1616668188.jpg', 'Support', NULL, NULL, 7000, 15000, '25-50', NULL, '90 mins', 7000, 15000, 'Open', NULL, '[1]', NULL, '[1]', '[1]', 1, 'wellness-stretching-fun.html', '2021-03-25 03:29:48'),
(320, 'Magic of Breathing and Relaxation', 'Carry physical, mental, and spiritual mindfulness into the workspace with this session on the magic of breathing and relaxation.', NULL, '/image/wellness-magic-of-breathing-and-relaxation-0.91380500-1616668391.jpg', 'Support', NULL, NULL, 7000, 15000, '25-50', NULL, '90 mins', 7000, 15000, 'Open', NULL, '[1]', NULL, '[1]', '[3,4,5]', 1, 'wellness-magic-of-breathing-and-relaxation.html', '2021-03-25 03:33:11'),
(321, 'Healing Touch', 'Havenâ€™t we all craved a massage in the middle of a high stress work day? Well, this workshop aims at providing that massage in a safe and mindful method, fitted for corporate environments. The luxury of a dry massage based on foot reflexology for a quick and relaxing fix for employeesâ€™ stressful office days.', NULL, '/image/wellness-healing-touch-0.82182600-1616668541.jpg', 'Support', NULL, NULL, 15000, 35000, '20-60', NULL, '6 hrs', NULL, NULL, 'null', NULL, '[1]', NULL, '[1]', '[5,4,1]', 1, 'wellness-healing-touch.html', '2021-03-25 03:35:41'),
(322, 'Fitness factor', 'We believe in individualised fitness solutions. For complete accuracy, a fitness analysis comprising of five fitness parameters is administered for each employee, and the necessary solutions suggested to reach their individual goals.', NULL, '/image/wellness-fitness-factor-0.07812300-1616668708.jpg', 'Support', NULL, NULL, 20000, 0, '30', NULL, '6 hrs', NULL, NULL, 'null', NULL, '[1]', NULL, '[1]', '[1,5,4]', 1, 'wellness-fitness-factor.html', '2021-03-25 03:38:28'),
(323, 'Way To Health', 'A wholesome unique workshop focusing on all aspects of health. Participants receive insights from experts in the field of medicine and fitness on the A-Zs of maintaining a high quality health lifestyle, encouraging them to proactively keep illness and other health related issues in check.', NULL, '/image/wellness-way-to-health-0.91802500-1616668843.jpg', 'Support', NULL, NULL, 22000, 0, 'Open', NULL, '90 mins', 22000, 0, 'Open', NULL, '[1]', NULL, '[1]', '[4,5,6]', 1, 'wellness-way-to-health.html', '2021-03-25 03:40:43'),
(324, 'Fun and Fitness Games', 'If your workouts feel as stressful as a quarterly report, somethingâ€™s not right. This session swings you back to childhood, and pumps fun and laughs back into fitness regiments.', NULL, '/image/wellness-fun-and-fitness-games-0.89365500-1616669010.jpg', 'Support', NULL, NULL, 20000, 0, 'Open', NULL, '4 hrs', NULL, NULL, 'null', NULL, '[1]', NULL, '[1]', '[6,5,4,1]', 1, 'wellness-fun-and-fitness-games.html', '2021-03-25 03:43:30'),
(325, 'Happiness Workshop', 'To access highest productivity potential, oneâ€™s mind needs to embrace honest happiness and peace. This workshop leads you through a journey to help reach your meta mind.', NULL, '/image/wellness-happiness-workshop-0.47157400-1616669191.jpg', 'Support', NULL, NULL, 25000, 50000, 'Open', NULL, '90 mins', 25000, 50000, 'Open', NULL, '[1]', NULL, '[1]', '[4,5]', 1, 'wellness-happiness-workshop.html', '2021-03-25 03:46:31'),
(326, 'Mini Master chef', 'Find out who has secret spices up their sleeves! A culinary challenge amongst employees for some fun, and a way to learn yummy healthy recipes.', NULL, '/image/wellness-mini-master-chef-0.72407500-1616669537.jpg', 'Support', NULL, NULL, 20000, 0, 'Upto 50', NULL, '3 hrs', NULL, NULL, 'null', NULL, '[1]', NULL, '[1]', '[2,4,5,6]', 1, 'wellness-mini-master-chef.html', '2021-03-25 03:52:17'),
(327, 'Nutrition Exhibition', 'As exciting as the internet is, ask our trained nutritionists instead of your Google browser. Theyâ€™ll have honest answers specific to your personal lifestyle and tastes.', NULL, '/image/wellness-nutrition-exhibition-0.92739600-1616670857.jpg', 'Support', NULL, NULL, 15000, 0, 'Open', NULL, '4 hrs', NULL, NULL, 'null', NULL, '[1]', NULL, '[1]', '[2,6]', 1, 'wellness-nutrition-exhibition.html', '2021-03-25 04:14:17'),
(328, 'Microgreens Workshop', 'Thereâ€™s nothing as special as growing your own food. A workshop on how to grow and nurture your own greens at home, and how to make delicious meals honouring them.', NULL, '/image/wellness-microgreens-workshop-0.35344100-1616671176.jpg', 'Support', NULL, NULL, 15000, 25000, '25 (Rs 350/- extra per person for additional participants)', NULL, '2 hrs', 15000, 25000, 'Open', NULL, '[1]', NULL, '[1]', '[4,5,6]', 1, 'wellness-microgreens-workshop.html', '2021-03-25 04:19:36'),
(329, 'Dance with Emotions', 'A session of dance movement to unblock the body and mind, gaining greater inner perspective.', NULL, '/image/wellness-dance-with-emotions-0.56036200-1616671400.jpg', 'Support', NULL, NULL, 25000, 35000, '25-50', NULL, '90 mins', 25000, 35000, 'Open', NULL, '[1]', NULL, '[1]', '[4,5,1,3]', 1, 'wellness-dance-with-emotions.html', '2021-03-25 04:23:20'),
(330, 'Sound Bowl Healing', 'Combining sound vibrations and meditation, this workshop instills stillness, peace and alignment in your inner environment through the Tibetan art of Sound Bowl Healing.', NULL, '/image/wellness-sound-bowl-healing-0.75323400-1616671703.jpg', 'Support', NULL, NULL, 15000, 25000, '20-30', NULL, '60 mins', 15000, 25000, 'Open', NULL, '[1]', NULL, '[1]', '[4,5,3]', 1, 'wellness-sound-bowl-healing.html', '2021-03-25 04:28:23'),
(331, 'Suhani Shaam', 'A fun filled evening with evergreen songs and music from bollywood and some anecdotes.', NULL, '/image/wellness-suhani-shaam-0.53763700-1616672246.jpg', 'Support', NULL, NULL, 30000, 100000, 'Open', NULL, '90 mins', 30000, 100000, 'Open', NULL, '[1]', NULL, '[1]', '[4,5,6]', 1, 'wellness-suhani-shaam.html', '2021-03-25 04:37:26'),
(332, 'Bar Chef', 'A true embodiment of the BarChef approach; creating custom cocktail/mocktails experiences through aromatics, flavour pairings and visual displays.', NULL, '/image/wellness-bar-chef-0.79759900-1616672341.jpg', 'Support', NULL, NULL, 30000, 40000, 'Open', NULL, '30-45 mins', 30000, 40000, 'Open', NULL, '[1]', NULL, '[1]', '[6]', 1, 'wellness-bar-chef.html', '2021-03-25 04:39:01'),
(333, 'Juggling Joy', 'Juggling or even watching someone juggle is a great way to escape any worries, stress, hardships, or anything that might be hanging over your head. It\'s great entertainment and a playful stress buster. Let our expert take over to show and teach some real tricks to your teams .', NULL, '/image/wellness-juggling-joy-0.48101700-1616672461.jpg', 'Support', NULL, NULL, 30000, 35000, 'Open', NULL, '30-45 mins', 30000, 35000, 'Open', NULL, '[1]', NULL, '[1]', '[6]', 1, 'wellness-juggling-joy.html', '2021-03-25 04:41:01'),
(334, 'Mind Games', 'Imagine a show that blurs the line between illusion and reality. One that plays with your mind and imagination leaving you in absolute shock and surprise. What if a Mentalism Show was brought right to your employees?', NULL, '/image/wellness-mind-games-0.83501600-1616672574.jpg', 'Support', NULL, NULL, 20000, 30000, 'Open', NULL, '1 hr', 20000, 30000, 'Open', NULL, '[1]', NULL, '[1]', '[4,5,6]', 1, 'wellness-mind-games.html', '2021-03-25 04:42:54'),
(335, 'Parenting Workshop', 'An Interactive sessions involving parents for good parenting practices and relationship building with children.', NULL, '/image/wellness-parenting-workshop-0.39055000-1616672668.jpg', 'Support', NULL, NULL, 15000, 25000, 'Open', NULL, '90 mins', 15000, 25000, 'Open', NULL, '[1]', NULL, '[1]', '[4,5]', 1, 'wellness-parenting-workshop.html', '2021-03-25 04:44:28'),
(336, 'Nutri Fun', 'Take your love for cooking nutritious a few notches higher with handpicked recieps demonstarated by our experts. Dips & Salads, Guilt free desserts, One Bowl meal and many more.', NULL, '/image/wellness-nutri-fun-0.78320000-1616672766.jpg', 'Support', NULL, NULL, 7000, 15000, 'Open', NULL, '90 mins', 7000, 15000, 'Open', NULL, '[1]', NULL, '[1]', '[4,5,6,2]', 1, 'wellness-nutri-fun.html', '2021-03-25 04:46:06'),
(337, 'Musical Radio Evening', 'This is a fun evening. Tune-in for an exciting musical adventure with our unique musicians, raring to go live and perform an exciting and entertaining line up for you.', NULL, '/image/wellness-musical-radio-night-0.88727200-1616672870.jpg', 'Support', NULL, NULL, 20000, 50000, 'Open', NULL, '30-60 mins', 20000, 50000, 'Open', NULL, '[1]', NULL, '[1]', '[6,5,4]', 1, 'wellness-musical-radio-evening.html', '2021-03-25 04:47:50'),
(338, 'Art Therapy', 'Pablo Picasso once said \"Art washes away from the soul the dust of everyday life.\" So just grab some pen and paper to join this unique session of expressing emotions and creating happiness. Example: Coffee Painting, Dot Mandala, Doodling, Finger painting, Art and emotions, Best out of waste etc', NULL, '/image/wellness-art-therapy-0.21075100-1616672991.jpg', 'Support', NULL, NULL, 10000, 20000, 'Open', NULL, '60 mins', 10000, 20000, 'Open', NULL, '[1]', NULL, '[1]', '[5,6,4]', 1, 'wellness-art-therapy.html', '2021-03-25 04:49:51'),
(339, 'Music Movement Meditation', 'Can Meditation be Fun? Can we Meditate without sitting at one place? Do we need to be quite to be focused? Specially curated session based on three elements for managing mental wellbeing.', NULL, '/image/wellness-music-movement-meditation-0.27445100-1616673117.jpg', 'Support', NULL, NULL, 15000, 25000, 'Open', NULL, '60 mins', 15000, 25000, 'Open', NULL, '[1]', NULL, '[1]', '[6,5,4]', 1, 'wellness-music-movement-meditation.html', '2021-03-25 04:51:57'),
(340, 'Chai Time', 'Storytellers brings some live entertainment to your evening chai time break! Bring your cup of tea, relax and enjoy stories in the form of music and poetry to make you laugh, smile, go \'aww\' and feel connected to those near or far away from you. Leave feeling refreshed to conquer the rest of your day with all smiles.', NULL, '/image/wellness-chai-time-0.85993700-1616673197.jpg', 'Support', NULL, NULL, 10000, 30000, 'Open', NULL, '30-60 mins', 10000, 30000, 'Open', NULL, '[1]', NULL, '[1]', '[6,5,4]', 1, 'wellness-chai-time.html', '2021-03-25 04:53:17'),
(341, 'Bollywood Dance', 'Bollywood dance is very high energy and upbeat with movement that boosts oxygen supply to key muscles, providing great aerobic exercise and improving muscular resistance over time.', NULL, '/image/wellness-bollywood-dance-0.08920300-1616838889.jpg', 'Support', NULL, NULL, 5000, 0, 'Upto 30', NULL, '60 mins', 5000, 0, 'Open', NULL, '[1]', NULL, '[1]', '[3,4,5,6]', 2, 'wellness-bollywood-dance.html', '2021-03-27 02:54:49');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `emailcode` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `name`, `emailcode`) VALUES
(1, 'veebee@i.in', ''),
(2, 'veebee@i.in', ''),
(3, 'veebee@i.in', ''),
(4, 'veebee@i.in', ''),
(5, 'tr@i.in', ''),
(6, 'err@i.in', 'Wprmyq3dRQ'),
(7, 'wwwrr@i.in', 'nr0v3gtjm6'),
(8, 'ewwwrr@i.in', 'NgSO2A4Xdu'),
(9, 'ewwwrr@i.in', '8ZlWldBn8C'),
(10, 'ewwwrr@i.in', 'Z4jcSAL2x5'),
(11, 'ewwwrr@i.in', 'oRxdBmFT7i'),
(12, 'ewwwrr@i.in', 'KzMeozgLx0'),
(13, 'ewwwrr@i.in', 'uH9BiWAcVH'),
(14, 'ewwwrr@i.in', 'jIb9MGkDtG'),
(15, 'ewwwrr@i.in', 'XkoLLuluPe'),
(16, 'ewwwrr@i.in', 'bFV815454T'),
(17, 'ewwwrr@i.in', 'VrJC5V3D3I'),
(18, '23wwrr@i.in', 'EfBM1IhDzX'),
(19, '‘lS÷1¥“GsH?˜Úö', ''),
(20, '‘lS÷1¥“GsH?˜Úö', ''),
(21, 'UŽË»²QZ3PhŒ<', ''),
(22, 'UŽË»²QZ3PhŒ<', ''),
(23, 'l-X{¤ÿqîá8Gv3œŸ', ''),
(24, 'mÙ@Å~\"q0Ÿ.!6³¶š²', ''),
(25, 'mÙ@Å~\"q0Ÿ.!6³¶š²', ''),
(26, '~£/÷mq—ÀB”`ƒ_', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` text,
  `emailcode` varchar(255) DEFAULT NULL,
  `Role` varchar(100) NOT NULL DEFAULT 'U',
  `designation` varchar(100) DEFAULT NULL,
  `mobile` text,
  `alternative_mobile` varchar(255) NOT NULL DEFAULT '0',
  `toContactNumber` tinyint(1) NOT NULL DEFAULT '0',
  `OTP` double NOT NULL DEFAULT '0',
  `company` int(11) NOT NULL DEFAULT '1',
  `city` int(11) NOT NULL DEFAULT '1',
  `location` int(11) NOT NULL DEFAULT '6',
  `token` text,
  `email_verified_status` varchar(10) DEFAULT 'N',
  `OTP_verified_status` varchar(10) DEFAULT 'N',
  `login_counter` int(11) DEFAULT '0',
  `otp_counter` int(11) DEFAULT '0',
  `lastEmailCodeTime` bigint(20) DEFAULT NULL,
  `lastOtpTime` bigint(20) DEFAULT NULL,
  `last_login` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `emailcode`, `Role`, `designation`, `mobile`, `alternative_mobile`, `toContactNumber`, `OTP`, `company`, `city`, `location`, `token`, `email_verified_status`, `OTP_verified_status`, `login_counter`, `otp_counter`, `lastEmailCodeTime`, `lastOtpTime`, `last_login`, `created_at`) VALUES
(1, 'ÚW}£&ê7¤z]Õ!ÊÐÇ', 'ÜOsÅ/žptÆóg	|-Rz`qh‹ÜD¤Ï…ÓIŒÃ', 'b8ecaa145cf72c8c1d4aa6f04978db2e', 'uAwXONmZbv', 'A', 'Facility manager', 'xçÃN&=‡*T[ñ½k¸', '0', 0, 0, 70, 619, 38, '106426351937938', 'Y', 'N', 0, 0, 1614240999, NULL, '1617005290', '2021-02-25 13:46:41'),
(2, '/úiâ\\J#ðT¢¹Nµ', '\r~ÔãóÔ1íÚ½üÜ-z`qh‹ÜD¤Ï…ÓIŒÃ', '4f5a54945413b3de5a13bb51cf69aff3', 'bEez2uJJD6XIHwNuDxkq', 'S', 'Wellness manager', 'xçÃN&=‡*T[ñ½k¸', '0', 0, 0, 70, 619, 38, '911508484822862', 'Y', 'N', 0, 0, 1619417705, NULL, '1619761374', '2021-02-25 13:49:01'),
(3, 'šç\r~òÎ=.™œ³ppÇfã', '³™š2ŠmÄd^°ç­!EÈ˜', '599c2b21543e5d9683f8ec4e918db939', 'noVwG6WaXq', 'U', 'Manager', '78+™\ZpM”Ç]ì	«Œp', '­¶Éwà—9Ê\'ÐMx¬', 0, 817110, 12, 857, 38, '220598943956484', 'Y', 'Y', 1, 0, 1614241328, NULL, '1617094886', '2021-02-25 13:52:10'),
(4, NULL, '´ÜëöAßc…‹Ùÿ5Ù}ìy`½ú3VÛzTÝ·#€', NULL, '7yuNkkKbn9K2edgRdOOL5e7F2', 'U', NULL, NULL, '0', 0, 0, 1, 1, 6, NULL, 'N', 'N', 0, 0, 1614324314, NULL, NULL, '2021-02-26 00:25:14'),
(5, 'W:¡^³â\\vŽ¥Fà}í', 'æDê8š¾=\",+ÉtÒÛÈõiÐ”¶ˆMŽ›±˜ûÙá', 'b616e89a06df5d1b6aeb257f67ccf596', 'oX9hPRUKnX9IRSOrY6OX8rnHvDytnT', 'U', 'Employee', 'hÉôDYºÍ‰¤Ñ?Ö¢š', '×ŠB\'M‡ÙÞZãÆÝj=­', 1, 718632, 12, 857, 38, '67885773870593', 'Y', 'Y', 0, 0, 1616063432, 1619848015, '1616066975', '2021-02-26 01:07:25'),
(7, '3Ã\"ÒÊwå!on€]Z¹\'', '}Ë‘ÛÍÉ\nBla;Ü¦Búr[DàONÖÄ-NZñÝn', '308fb60115dbc5decc481c0f99becd47', 'SMVvLyAJUZUFMEqbPYp5', 'U', 'Wellness manager', 'ž8–ÖüwÝÓ™Öù]FR9', '0', 0, 0, 32, 2, 18, '461131572807482', 'Y', 'N', 0, 0, 1619777232, NULL, '1619697099', '2021-03-01 00:53:49'),
(8, 'ÃKÈ4}RÃ?TDž¶í3è', 'E_–¯ú&¼þŠY½”Ér[DàONÖÄ-NZñÝn', 'dbc68d0c4aa1d72fdc03cac70f04e9ad', 'bQeFdwnrlO', 'U', 'Employee', 'Cçlýé%¡þ¥Ú}—Wù', '0', 0, 0, 12, 857, 37, '363361886467143', 'Y', 'N', 1, 0, 1615975756, NULL, '1616391161', '2021-03-17 03:09:21'),
(13, 'AÃii±úQ1}»g5X€', 'Ã_ö\"³Ïµ€ªK‚rßŸ3\0°òS\ZØ¨Ùn @t5Ø”', 'df473fa0a10fa281f51f0d76507095e3', 'zkdJIBZ4qK', 'U', 'Employee', 'Ó}/‚ÐA ±WÏÉ&Ä~', 'WãœêEð‡F…!³BK', 1, 455233, 74, 857, 38, '988687967207860', 'Y', 'Y', 0, 0, 1616146746, 1616147489, NULL, '2021-03-19 02:39:12'),
(15, 'Úíô|¬o*x:ãEm‹ºÿ', '”AG‘¢jŸ¬{3¸G—ªïÇ|6ªo[Ž­KÀ?Sý¤', '4f5a54945413b3de5a13bb51cf69aff3', 'Vg5mTWrVeM', 'A', 'Admin', 'xçÃN&=‡*T[ñ½k¸', '0', 0, 0, 70, 619, 29, '727744851154341', 'Y', 'N', 0, 0, 1616413184, NULL, NULL, '2021-03-22 04:39:49'),
(16, 'š–ß\n³ækí²(¶£ç¢T', '?œ¤p5G¹Ü›”Ëšs…‹2±åŠ9|=3êU7ôCÎ', '637534835cb265c5e844f6d2e76a79b0', 'a3QZC0sfTc', 'A', 'Admin', '‹êl+–ŠâØMVCÝ«j', '0', 0, 0, 74, 619, 38, '475682444078650', 'Y', 'N', 0, 0, 1619765537, NULL, NULL, '2021-04-25 23:46:29'),
(17, NULL, 'E¿2¢»¸cPå²†‚ÒIŸnì +y_Šàf', NULL, '4sYWh5jdLX0uBHwnHKb2T58T9', 'A', NULL, NULL, '0', 0, 0, 1, 1, 6, NULL, 'N', 'N', 0, 0, 1619597053, NULL, NULL, '2021-04-28 01:04:18'),
(18, 'W:¡^³â\\vŽ¥Fà}í', 'j7”/o¬ŸÜ=`\Zòr[DàONÖÄ-NZñÝn', 'b616e89a06df5d1b6aeb257f67ccf596', 'sskud1QdJQ', 'U', 'Employee', 'hÉôDYºÍ‰¤Ñ?Ö¢š', '0', 0, 0, 12, 857, 38, NULL, 'Y', 'N', 0, 0, 1619601913, NULL, NULL, '2021-04-28 02:25:18'),
(19, NULL, 'Ü8ÎŠÝÒ=ñ	N]3\0°òS\ZØ¨Ùn @t5Ø”', NULL, 'YHyRMJliRf2locrmcQn2aVWGI', 'U', NULL, NULL, '0', 0, 0, 1, 1, 6, NULL, 'N', 'N', 0, 0, 1619619565, NULL, NULL, '2021-04-28 07:02:06'),
(20, NULL, ':¼£þ°\'‘Žqˆ\0àTr[DàONÖÄ-NZñÝn', NULL, '93H4tYnowTxvuiYHYzbNpsAox', 'U', NULL, NULL, '0', 0, 0, 1, 1, 6, NULL, 'N', 'N', 0, 0, 1619670784, NULL, NULL, '2021-04-28 21:30:00'),
(21, 'apgÇ§èYæ¨\náH', 'é8\"×C´þyŒguºçÈ£fuvˆE‚>Pÿ‰}»Ý\0_', '897952812ee3f48a10b917b06b1d5ee0', 'Zvsz3jjiPG', 'U', 'Employee', '×ŠB\'M‡ÙÞZãÆÝj=­', '0', 0, 600446, 32, 348, 27, '4738178787274', 'Y', 'Y', 0, 0, 1619671254, NULL, NULL, '2021-04-28 21:40:59'),
(25, NULL, 'ý¢9ÁÅ\Z;q¨ÿ€®%§.•>u7˜g2»ßu?0', NULL, 'EsxvZl6O3d2EHZz8XKNDKQsao', 'U', NULL, NULL, '0', 0, 0, 1, 1, 6, NULL, 'N', 'N', 0, 0, 1619683706, NULL, NULL, '2021-04-29 01:08:31'),
(28, 'çÈ›Ì%&û\ZÊÒ:œ', 'ËK<ÒC%¸Ëû€úÅ	öo3\0°òS\ZØ¨Ùn @t5Ø”', '308fb60115dbc5decc481c0f99becd47', 'SZ3rtnu1Kv8ta8dC6xVT', 'U', 'Facility manager', 'ž8–ÖüwÝÓ™Öù]FR9', '0', 0, 0, 74, 128, 6, '540946844819514', 'Y', 'N', 0, 0, 1619698969, NULL, NULL, '2021-04-29 02:44:49'),
(29, NULL, 'ËK<ÒC%¸Ëû€úÅ	öoIeý±xb“XXyBÄ¬Æ', NULL, 'foYWn13B0vF40DNAHVDOW1WeM', 'U', NULL, NULL, '0', 0, 0, 1, 1, 6, NULL, 'N', 'N', 0, 0, 1619697349, NULL, NULL, '2021-04-29 04:55:54'),
(30, NULL, 'ËK<ÒC%¸Ëû€úÅ	öo·´íƒDtâ:‰[êàÓ¿', NULL, 'H13UgydPJxdm9DV5zBdg5CR3M', 'U', NULL, NULL, '0', 0, 0, 1, 1, 6, NULL, 'N', 'N', 0, 0, 1619697374, NULL, NULL, '2021-04-29 04:56:19'),
(31, NULL, 'ËK<ÒC%¸Ëû€úÅ	öo?{\"öÚm•_)bcæÜÆ´', NULL, 'YnMm8nxYg7FctAQy2fdqKa1rL', 'U', NULL, NULL, '0', 0, 0, 1, 1, 6, NULL, 'N', 'N', 0, 0, 1619698161, NULL, NULL, '2021-04-29 05:09:26'),
(33, NULL, 'ÈÆB£p\ZãG\"µ\'oks,~€îÃ+=ÝJ4Z\'!Í', NULL, 'trrdH1I7Zm13r30ju0RxviLpO', 'A', NULL, NULL, '0', 0, 0, 1, 1, 6, NULL, 'N', 'N', 0, 0, 1619702785, NULL, NULL, '2021-04-29 06:08:35'),
(34, 'ûâù¤xëx:fB³$', 'ú¸ú;ß%ß1Š+’F”È(fuvˆE‚>Pÿ‰}»Ý\0_', 'e9128053eaae8c2169bf44bc4d60e894', 'Y9rFlCQPq0', 'A', 'Admin', 'xçÃN&=‡*T[ñ½k¸', '0', 0, 0, 70, 619, 29, NULL, 'Y', 'N', 0, 0, 1619705327, NULL, NULL, '2021-04-29 07:08:52'),
(35, '{¬hÂâñâ³UÊ®;\n¯', 'rÊsö\'`BÂö™Œ ßœœÙpó(¹_.ušÜLÖºr', 'd31d675365e11a72722eb5b5dcf4ff0a', 'jl7O4ZGos3', 'U', 'Chief Executive Officer', '­¶Éwà—9Ê\'ÐMx¬', '0', 0, 532391, 74, 619, 38, '213169906213394', 'Y', 'Y', 0, 0, 1619705389, NULL, '1619705618', '2021-04-29 07:09:55'),
(36, NULL, '`b<à?^Žmv,§‘ô4XôbÝp’hàÓ!€RV¢äá', NULL, '6tSyAjA98yS4xaWcsmmixZ8aV', 'U', NULL, NULL, '0', 0, 0, 1, 1, 6, NULL, 'N', 'N', 0, 0, 1619705891, NULL, NULL, '2021-04-29 07:18:16'),
(37, '<´ÊØ‰ÑiÕ§7~ÿ–Å:', 'N¶z‹·æ4ï–ý$¼)éßz`qh‹ÜD¤Ï…ÓIŒÃ', 'e9128053eaae8c2169bf44bc4d60e894', 'yhi46Wg2cY', 'U', 'HR', '=0ªT”ø-Â™¾~<dYÈˆ', '0', 0, 459184, 70, 619, 38, '864091617989732', 'Y', 'Y', 0, 0, 1620730394, NULL, NULL, '2021-05-11 03:53:14'),
(38, ':	Ã‰0{·>«\"tÈøä', '=`?bÈy	àÈ#èÂ\"5Œß4bÞj~ï(/í´ïÀ?M±QŠŠ‡vñS', '88e7332a009d3f6626fab14fc999c7f8', 'tkA1Jcy37X', 'U', 'Chief Executive Officer', 'ÃâGÁ¼\'}ÑëýôÝF]Ôº', '0', 0, 160800, 70, 619, 29, '941532106670450', 'Y', 'Y', 0, 0, 1620731163, NULL, NULL, '2021-05-11 04:05:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog_details`
--
ALTER TABLE `blog_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_details_ibfk_1` (`blogId`);

--
-- Indexes for table `blog_list`
--
ALTER TABLE `blog_list`
  ADD PRIMARY KEY (`blogId`);

--
-- Indexes for table `budget_master`
--
ALTER TABLE `budget_master`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `budget_range` (`budget_range`);

--
-- Indexes for table `category_level1`
--
ALTER TABLE `category_level1`
  ADD PRIMARY KEY (`level1_id`);

--
-- Indexes for table `category_level2`
--
ALTER TABLE `category_level2`
  ADD PRIMARY KEY (`level2_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `city_name` (`city_name`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_name` (`company_name`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `engagement_master`
--
ALTER TABLE `engagement_master`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `engagement_type` (`engagement_type`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `final_services`
--
ALTER TABLE `final_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frequency_master`
--
ALTER TABLE `frequency_master`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `frequent_range` (`frequent_range`);

--
-- Indexes for table `gallery_list`
--
ALTER TABLE `gallery_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_list`
--
ALTER TABLE `image_list`
  ADD PRIMARY KEY (`imageId`),
  ADD KEY `galleryId` (`galleryId`);

--
-- Indexes for table `image_thumbnail`
--
ALTER TABLE `image_thumbnail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`leadId`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `location` (`location_name`),
  ADD KEY `location_name` (`location_name`);

--
-- Indexes for table `member_master`
--
ALTER TABLE `member_master`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `members` (`members`);

--
-- Indexes for table `r_frequancy`
--
ALTER TABLE `r_frequancy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selected_services`
--
ALTER TABLE `selected_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services_master`
--
ALTER TABLE `services_master`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `level1_cat` (`level1_cat`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_ibfk_1` (`company`),
  ADD KEY `city` (`city`),
  ADD KEY `location` (`location`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog_details`
--
ALTER TABLE `blog_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `blog_list`
--
ALTER TABLE `blog_list`
  MODIFY `blogId` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `budget_master`
--
ALTER TABLE `budget_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category_level1`
--
ALTER TABLE `category_level1`
  MODIFY `level1_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category_level2`
--
ALTER TABLE `category_level2`
  MODIFY `level2_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1014;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `engagement_master`
--
ALTER TABLE `engagement_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `final_services`
--
ALTER TABLE `final_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `frequency_master`
--
ALTER TABLE `frequency_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `gallery_list`
--
ALTER TABLE `gallery_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `image_list`
--
ALTER TABLE `image_list`
  MODIFY `imageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;

--
-- AUTO_INCREMENT for table `image_thumbnail`
--
ALTER TABLE `image_thumbnail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `leadId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `member_master`
--
ALTER TABLE `member_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `r_frequancy`
--
ALTER TABLE `r_frequancy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `selected_services`
--
ALTER TABLE `selected_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `services_master`
--
ALTER TABLE `services_master`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=342;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_details`
--
ALTER TABLE `blog_details`
  ADD CONSTRAINT `blog_details_ibfk_1` FOREIGN KEY (`blogId`) REFERENCES `blog_list` (`blogId`) ON DELETE CASCADE;

--
-- Constraints for table `image_list`
--
ALTER TABLE `image_list`
  ADD CONSTRAINT `image_list_ibfk_1` FOREIGN KEY (`galleryId`) REFERENCES `gallery_list` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `leads`
--
ALTER TABLE `leads`
  ADD CONSTRAINT `leads_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

--
-- Constraints for table `services_master`
--
ALTER TABLE `services_master`
  ADD CONSTRAINT `services_master_ibfk_1` FOREIGN KEY (`level1_cat`) REFERENCES `category_level1` (`level1_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`company`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`city`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`location`) REFERENCES `location` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
