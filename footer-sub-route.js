document.write(`<div class="container">
<div class="row">
    <div class="col-lg-4 col-md-12">
        <div class="footer__about">
            <img title="Wellness Associates Logo" src="../images/wellness/wellnesslogo.png" alt="Wellness Associates Logo">
            <!-- <ul>
                <li><i class="fa fa-clock-o"></i> Mon - Fri: 6:30am - 07:45pm</li>
                <li><i class="fa fa-clock-o"></i> Sat - Sun: 8:30am - 05:45pm</li>
            </ul> -->
            
        </div>
    </div>
    <div class="col-lg-2 offset-lg-1 col-md-3 col-sm-6 d-none d-lg-block">
        <div class="footer__widget">
            <h5>Website</h5>
            <ul>
                <li><a href="../">Home</a></li>
                <li><a href="../#services">Services</a></li>
                <li><a href="../about-us.html">About</a></li>
                <li><a href="../contact.html">Contact</a></li>
                <li><a href="../blogs.html">Blog</a></li>
                <!-- <li><a href="../sitemap.html">Sitemap</a></li> -->

            </ul>
        </div>
    </div>
    <div class="col-lg-2 col-md-12 col-sm-12 d-none d-lg-block">
        <div class="footer__widget">
            <h5>Our Services</h5>
            <ul>
                <li><a href="../workshops.html">Workshops And Events</a></li>
                <li><a href="../groupactivities.html">Group Activities</a></li>
                <li><a href="../fitnessfacility.html">Fitness Facility</a></li>
                <li><a href="../clubhouse.html">Club House</a></li>
                <li><a href="../thenewme.html">thenewme+</a></li>
            </ul>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="footer__widget">
            <h5>Contact Us</h5>
            <ul class="footer-address">
                <li><i class="fa fa-phone"></i>  +91 22-26714410 </li>
                <li><i class="fa fa-mobile footermobileicon" aria-hidden="true" ></i>  +91-9820161008 </li>
                <li><i class="fa fa-envelope"></i> info@wellnessassociates.in</li>
                <li class="footaddress"><div class="row"><div class="col-1 mt-2"><i class="fa fa-location-arrow"></i></div><div class="col">Mumbai (Head Office) <br/> Operations Pan-India</div></div></li>

            </ul>
        </div>
    </div>
</div>


<!-- <div class="footer__copyright">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="footer__copyright__text">
                <p>Copyright ©<script>document.write(new Date().getFullYear());</script>2021 All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>. Downloaded from <a href="https://themeslab.org/" target="_blank">Themeslab</a></p>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="footer__copyright__social">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>
            </div>
        </div>
    </div>
</div> -->
</div>`);