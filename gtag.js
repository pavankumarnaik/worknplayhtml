var imported = document.createElement('script');
imported.async = true;
imported.src = "https://www.googletagmanager.com/gtag/js?id=G-KKDSGYFVP5"
document.head.appendChild(imported)

window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments) }
gtag('js', new Date());

gtag('config', 'G-KKDSGYFVP5');
